<?php
    define('TIMEZONE', 'America/Argentina/Buenos_Aires');
    date_default_timezone_set(TIMEZONE);

    require_once "./controllers/EntityAPI.php";
    require_once "./controllers/EntityDB.php";
    require_once "./controllers/BancoAPI.php";
    require_once "./controllers/BancoDB.php";
    require_once "./controllers/CargaSustratoAPI.php";
    require_once "./controllers/CargaSustratoDB.php";
    require_once "./controllers/CargaTipoAPI.php";
    require_once "./controllers/CargaTipoDB.php";
    require_once "./controllers/ChequeAPI.php";
    require_once "./controllers/ChequeDB.php";
    require_once "./controllers/ClienteAPI.php";
    require_once "./controllers/ClienteDB.php";
    require_once "./controllers/ClienteTipoAPI.php";
    require_once "./controllers/ClienteTipoDB.php";
    require_once "./controllers/CompraAPI.php";
    require_once "./controllers/CompraDB.php";
    require_once "./controllers/CompraItemAPI.php";
    require_once "./controllers/CompraItemDB.php";
    require_once "./controllers/DisenoAPI.php";
    require_once "./controllers/DisenoDB.php";
    require_once "./controllers/FormaCobroAPI.php";
    require_once "./controllers/FormaCobroDB.php";
    require_once "./controllers/FormaPagoAPI.php";
    require_once "./controllers/FormaPagoDB.php";
    require_once "./controllers/GastoAPI.php";
    require_once "./controllers/GastoDB.php";
    require_once "./controllers/GastoDeudaAPI.php";
    require_once "./controllers/GastoDeudaDB.php";
    require_once "./controllers/GastoItemAPI.php";
    require_once "./controllers/GastoItemDB.php";
    require_once "./controllers/GramajeAPI.php";
    require_once "./controllers/GramajeDB.php";
    require_once "./controllers/ItemDisenoAPI.php";
    require_once "./controllers/ItemDisenoDB.php";
    require_once "./controllers/ItemImpresionLaserAPI.php";
    require_once "./controllers/ItemImpresionLaserDB.php";
    require_once "./controllers/ItemPapeleriaAPI.php";
    require_once "./controllers/ItemPapeleriaDB.php";
    require_once "./controllers/ItemPresupuestoAPI.php";
    require_once "./controllers/ItemPresupuestoDB.php";
    require_once "./controllers/ListaPrecioAPI.php";
    require_once "./controllers/ListaPrecioDB.php";
    require_once "./controllers/LPrecioAdicionalAPI.php";
    require_once "./controllers/LPrecioAdicionalDB.php";
    require_once "./controllers/LPrecioCarteleriaAPI.php";
    require_once "./controllers/LPrecioCarteleriaDB.php";
    require_once "./controllers/LPrecioDisenoAPI.php";
    require_once "./controllers/LPrecioDisenoDB.php";
    require_once "./controllers/LPrecioImpresionLaserAPI.php";
    require_once "./controllers/LPrecioImpresionLaserDB.php";
    require_once "./controllers/LPrecioImpresionLaserIntervaloAPI.php";
    require_once "./controllers/LPrecioImpresionLaserIntervaloDB.php";
    require_once "./controllers/LPrecioPapeleriaAPI.php";
    require_once "./controllers/LPrecioPapeleriaDB.php";
    require_once "./controllers/LPrecioPapeleriaIntervaloAPI.php";
    require_once "./controllers/LPrecioPapeleriaIntervaloDB.php";
    require_once "./controllers/ModuloAPI.php";
    require_once "./controllers/ModuloDB.php";
    require_once "./controllers/ModuloXRolAPI.php";
    require_once "./controllers/ModuloXRolDB.php";
    require_once "./controllers/PrecioAPI.php";
    require_once "./controllers/PrecioDB.php";
    require_once "./controllers/PresupuestoAPI.php";
    require_once "./controllers/PresupuestoDB.php";
    require_once "./controllers/PresupuestoItAdCarteleriaAPI.php";
    require_once "./controllers/PresupuestoItAdCarteleriaDB.php";
    require_once "./controllers/PresupuestoItAdImpresionLaserAPI.php";
    require_once "./controllers/PresupuestoItAdImpresionLaserDB.php";
    require_once "./controllers/PresupuestoItAdPapeleriaAPI.php";
    require_once "./controllers/PresupuestoItAdPapeleriaDB.php";
    require_once "./controllers/PresupuestoItemCarteleriaAPI.php";
    require_once "./controllers/PresupuestoItemCarteleriaDB.php";
    require_once "./controllers/PresupuestoItemDisenoAPI.php";
    require_once "./controllers/PresupuestoItemImpresionLaserAPI.php";
    require_once "./controllers/PresupuestoItemImpresionLaserDB.php";
    require_once "./controllers/PresupuestoItemDisenoDB.php";
    require_once "./controllers/PresupuestoItemPapeleriaAPI.php";
    require_once "./controllers/PresupuestoItemPapeleriaDB.php";
    require_once "./controllers/ProveedorAPI.php";
    require_once "./controllers/ProveedorDB.php";
    require_once "./controllers/ReciboAPI.php";
    require_once "./controllers/ReciboDB.php";
    require_once "./controllers/ReciboItemAPI.php";
    require_once "./controllers/ReciboItemDB.php";
    require_once "./controllers/RegistroCajaAPI.php";
    require_once "./controllers/RegistroCajaDB.php";
    require_once "./controllers/ReporteAPI.php";
    require_once "./controllers/ReporteDB.php";
    require_once "./controllers/RetiroEfectivoAPI.php";
    require_once "./controllers/RetiroEfectivoDB.php";
    require_once "./controllers/RolAPI.php";
    require_once "./controllers/RolDB.php";
    require_once "./controllers/SubTipoDisenoAPI.php";
    require_once "./controllers/SubTipoDisenoDB.php";
    require_once "./controllers/SubTipoPapeleriaAPI.php";
    require_once "./controllers/SubTipoPapeleriaDB.php";
    require_once "./controllers/SubTipoPapeleriaComercialAPI.php";
    require_once "./controllers/SubTipoPapeleriaComercialDB.php";
    require_once "./controllers/SubTipoSustratoAPI.php";
    require_once "./controllers/SubTipoSustratoDB.php";
    require_once "./controllers/SustratoAPI.php";
    require_once "./controllers/SustratoDB.php";
    require_once "./controllers/TamanoAPI.php";
    require_once "./controllers/TamanoDB.php";
    require_once "./controllers/TipoAdicionalAPI.php";
    require_once "./controllers/TipoAdicionalDB.php";
    require_once "./controllers/TipoComprobanteAPI.php";
    require_once "./controllers/TipoComprobanteDB.php";
    require_once "./controllers/TipoDisenoAPI.php";
    require_once "./controllers/TipoDisenoDB.php";
    require_once "./controllers/TipoPapeleriaComercialAPI.php";
    require_once "./controllers/TipoPapeleriaComercialDB.php";
    require_once "./controllers/TipoPapeleriaCorteAPI.php";
    require_once "./controllers/TipoPapeleriaCorteDB.php";
    require_once "./controllers/TipoTrabajoPapeleriaAPI.php";
    require_once "./controllers/TipoTrabajoPapeleriaDB.php";
    require_once "./controllers/TipoSevicioAPI.php";
    require_once "./controllers/TipoSevicioDB.php";
    require_once "./controllers/UsuarioAPI.php";
    require_once "./controllers/UsuarioDB.php";
    /*
    $request_headers = apache_request_headers();
    $http_origin = $request_headers['Origin'];
    $allowed_http_origins = array(
        "http://localhost:4200",
        "http://localhost" ,
        "http://181.164.21.99");
    if (in_array($http_origin, $allowed_http_origins)){  
        header("Access-Control-Allow-Origin: " . $http_origin);
    }*/
	header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, PATCH');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Cache-Control, Pragma');
    
    $action = $_GET['action'];
    $api = NULL;
    
    switch($action)
    {
        case BancoAPI::API_ACTION:
            $api = new BancoAPI();
            break;
        case CargaSustratoAPI::API_ACTION:
            $api = new CargaSustratoAPI();
            break;
        case CargaTipoAPI::API_ACTION:
            $api = new CargaTipoAPI();
            break;
        case ChequeAPI::API_ACTION:
            $api = new ChequeAPI();
            break;
        case ClienteAPI::API_ACTION:
            $api = new ClienteAPI();
            break;
        case ClienteTipoAPI::API_ACTION:
            $api = new ClienteTipoAPI();
            break;
        case CompraAPI::API_ACTION:
            $api = new CompraAPI();
            break;
        case CompraItemAPI::API_ACTION:
            $api = new CompraItemAPI();
            break;
        case DisenoAPI::API_ACTION:
            $api = new DisenoAPI();
            break;
        case FormaCobroAPI::API_ACTION:
            $api = new FormaCobroAPI();
            break;
        case FormaPagoAPI::API_ACTION:
            $api = new FormaPagoAPI();
            break;
        case GastoAPI::API_ACTION:
            $api = new GastoAPI();
            break;
        case GastoDeudaAPI::API_ACTION:
            $api = new GastoDeudaAPI();
            break;
        case GastoItemAPI::API_ACTION:
            $api = new GastoItemAPI();
            break;
        case GramajeAPI::API_ACTION:
            $api = new GramajeAPI();
            break;
        case ItemDisenoAPI::API_ACTION:
            $api = new ItemDisenoAPI();
            break;
        case ItemImpresionLaserAPI::API_ACTION:
            $api = new ItemImpresionLaserAPI();
            break;
        case ItemPapeleriaAPI::API_ACTION:
            $api = new ItemPapeleriaAPI();
            break;
        case ItemPresupuestoAPI::API_ACTION:
            $api = new ItemPresupuestoAPI();
            break;
        case ListaPrecioAPI::API_ACTION:
            $api = new ListaPrecioAPI();
            break;
        case LPrecioAdicionalAPI::API_ACTION:
            $api = new LPrecioAdicionalAPI();
            break;
        case LPrecioCarteleriaAPI::API_ACTION:
            $api = new LPrecioCarteleriaAPI();
            break;
        case LPrecioDisenoAPI::API_ACTION:
            $api = new LPrecioDisenoAPI();
            break;
        case LPrecioImpresionLaserAPI::API_ACTION:
            $api = new LPrecioImpresionLaserAPI();
            break;
        case LPrecioImpresionLaserIntervaloAPI::API_ACTION:
            $api = new LPrecioImpresionLaserIntervaloAPI();
            break;
        case LPrecioPapeleriaAPI::API_ACTION:
            $api = new LPrecioPapeleriaAPI();
            break;
        case LPrecioPapeleriaIntervaloAPI::API_ACTION:
            $api = new LPrecioPapeleriaIntervaloAPI();
            break;
        case ModuloAPI::API_ACTION:
            $api = new ModuloAPI();
            break;
        case ModuloXRolAPI::API_ACTION:
            $api = new ModuloXRolAPI();
            break;
        case PrecioAPI::API_ACTION:
            $api = new PrecioAPI();
            break;
        case PresupuestoAPI::API_ACTION:
            $api = new PresupuestoAPI();
            break;
        case PresupuestoItAdCarteleriaAPI::API_ACTION:
            $api = new PresupuestoItAdCarteleriaAPI();
            break;
        case PresupuestoItAdPapeleriaAPI::API_ACTION:
            $api = new PresupuestoItAdPapeleriaAPI();
            break;
        case PresupuestoItemCarteleriaAPI::API_ACTION:
            $api = new PresupuestoItemCarteleriaAPI();
            break;
        case PresupuestoItAdImpresionLaserAPI::API_ACTION:
            $api = new PresupuestoItAdImpresionLaserAPI();
            break;
        case PresupuestoItemImpresionLaserAPI::API_ACTION:
            $api = new PresupuestoItemImpresionLaserAPI();
            break;
        case PresupuestoItemDisenoAPI::API_ACTION:
            $api = new PresupuestoItemDisenoAPI();
            break;
        case PresupuestoItemPapeleriaAPI::API_ACTION:
            $api = new PresupuestoItemPapeleriaAPI();
            break;
        case ProveedorAPI::API_ACTION:
            $api = new ProveedorAPI();
            break;
        case ReciboAPI::API_ACTION:
            $api = new ReciboAPI();
            break;
        case ReciboItemAPI::API_ACTION:
            $api = new ReciboItemAPI();
            break;
        case RegistroCajaAPI::API_ACTION:
            $api = new RegistroCajaAPI();
            break;
        case ReporteAPI::API_ACTION:
            $api = new ReporteAPI();
            break;
        case RetiroEfectivoAPI::API_ACTION:
            $api = new RetiroEfectivoAPI();
            break;
        case RolAPI::API_ACTION:
            $api = new RolAPI();
            break;
        case SubTipoDisenoAPI::API_ACTION:
            $api = new SubTIpoDisenoAPI();
            break;
        case SubTipoPapeleriaAPI::API_ACTION:
            $api = new SubTipoPapeleriaAPI();
            break;
        case SubTipoPapeleriaComercialAPI::API_ACTION:
            $api = new SubTipoPapeleriaComercialAPI();
            break;
        case SubTipoSustratoAPI::API_ACTION:
            $api = new SubTipoSustratoAPI();
            break;
        case SustratoAPI::API_ACTION:
            $api = new SustratoAPI();
            break;
        case TamanoAPI::API_ACTION:
            $api = new TamanoAPI();
            break;
        case TipoAdicionalAPI::API_ACTION:
            $api = new TipoAdicionalAPI();
            break;
        case TipoComprobanteAPI::API_ACTION:
            $api = new TipoComprobanteAPI();
            break;
        case TipoDisenoAPI::API_ACTION:
            $api = new TipoDisenoAPI();
            break;
        case TipoPapeleriaComercialAPI::API_ACTION:
            $api = new TipoPapeleriaComercialAPI();
            break;
        case TipoPapeleriaCorteAPI::API_ACTION:
            $api = new TipoPapeleriaCorteAPI();
            break;
        case TipoTrabajoPapeleriaAPI::API_ACTION:
            $api = new TipoTrabajoPapeleriaAPI();
            break;
        case TipoSevicioAPI::API_ACTION:
            $api = new TipoSevicioAPI();
            break;
        case UsuarioAPI::API_ACTION:
            $api = new UsuarioAPI();
            break;
        default:
            echo 'METODO NO SOPORTADO';
            break;
    }
    
    if ($api != NULL) {
        $api->API();
    }
