<?php
/**
 * Description of PresupuestoItemPapeleriaDB
 *
 * @author meza
 */
class PresupuestoItemPapeleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestositemspapeleria';
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idpresupuesto, 
            i.idtipotrabajopapeleria, t.tipotrabajopapeleria, 
            i.idgramaje, g.gramaje, i.idtamano, n.tamano, 
            i.idtipopapelcom, c.tipopapeleriacomercial, i.idsubpapelcom, s.subtipopapeleriacomercial, 
            i.idtipocorte, r.tipocorte, i.unidades, i.descripcion, i.pliegos, i.doblefaz,
            i.precio, i.color, i.fecultmodif 
                FROM presupuestositemspapeleria i 
                LEFT JOIN tipospapeleriastrabajos t ON i.idtipotrabajopapeleria = t.id 
                LEFT JOIN gramajes g ON i.idgramaje = g.id
                LEFT JOIN tamanos n ON i.idtamano = n.id
                LEFT JOIN tipospapeleriascomerciales c ON i.idtipopapelcom = c.id
                LEFT JOIN subtipospapeleriascomerciales s ON i.idsubpapelcom = s.id
                LEFT JOIN tipospapeleriascortes r ON i.idtipocorte = r.id 
            WHERE i.id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT i.id, i.idpresupuesto, 
            i.idtipotrabajopapeleria, t.tipotrabajopapeleria, 
            i.idgramaje, g.gramaje, i.idtamano, n.tamano, 
            i.idtipopapelcom, c.tipopapeleriacomercial, 
            i.idsubpapelcom, s.subtipopapeleriacomercial, 
            i.idtipocorte, r.tipocorte, i.unidades, i.descripcion, i.pliegos, i.doblefaz, 
            i.precio, i.color, i.fecultmodif 
                FROM presupuestositemspapeleria i 
                LEFT JOIN tipospapeleriastrabajos t ON i.idtipotrabajopapeleria = t.id 
                LEFT JOIN gramajes g ON i.idgramaje = g.id
                LEFT JOIN tamanos n ON i.idtamano = n.id
                LEFT JOIN tipospapeleriascomerciales c ON i.idtipopapelcom = c.id
                LEFT JOIN subtipospapeleriascomerciales s ON i.idsubpapelcom = s.id
                LEFT JOIN tipospapeleriascortes r ON i.idtipocorte = r.id;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPresupuesto($idpresupuesto) {
        $query = "SELECT i.id, i.idpresupuesto, 
            i.idtipotrabajopapeleria, t.tipotrabajopapeleria, 
            i.idgramaje, g.gramaje, i.idtamano, n.tamano, 
            i.idtipopapelcom, c.tipopapeleriacom, i.idsubpapelcom, s.subtipopapeleriacom, 
            i.idtipocorte, r.tipocorte, i.unidades, i.descripcion, i.pliegos, i.doblefaz, 
            i.precio, i.color, i.fecultmodif 
                FROM presupuestositemspapeleria i 
                LEFT JOIN tipospapeleriastrabajos t ON i.idtipotrabajopapeleria = t.id 
                LEFT JOIN gramajes g ON i.idgramaje = g.id
                LEFT JOIN tamanos n ON i.idtamano = n.id
                LEFT JOIN tipospapeleriascomerciales c ON i.idtipopapelcom = c.id
                LEFT JOIN subtipospapeleriascomerciales s ON i.idsubpapelcom = s.id
                LEFT JOIN tiposcortes r ON i.idtipocorte = r.id 
                WHERE i.idpresupuesto = $idpresupuesto";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idpresupuesto, $idtipotrabajopapeleria, $idgramaje, 
            $idtamano, $idtipopapelcom, $idsubpapelcom, 
            $idtipocorte, $unidades, $descripcion, 
            $pliegos, $doblefaz, $precio, $color){
        
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitpapeleria(0,-1,
                $idpresupuesto, $idtipotrabajopapeleria, $idgramaje,
                $idtamano, $idtipopapelcom, $idsubpapelcom,
                $idtipocorte, $unidades, '$descripcion',
                $pliegos, $doblefaz, $precio,
                $color, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function update($id, 
            $idpresupuesto, $idtipotrabajopapeleria, $idgramaje, 
            $idtamano, $idtipopapelcom, $idsubpapelcom, 
            $idtipocorte, $unidades, $descripcion, 
            $pliegos, $doblefaz, $precio, $color) {
        
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitpapeleria(1, $id,
                $idpresupuesto, $idtipotrabajopapeleria, $idgramaje,
                $idtamano, $idtipopapelcom, $idsubpapelcom,
                $idtipocorte, $unidades, '$descripcion',
                $pliegos, $doblefaz, $precio,
                $color, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitpapeleria(2, $id,
                0, 0, 0, 
                0, 0, 0, 
                0, 0, '',
                0, 0, 0,  
                0, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return 0;
    }
}