<?php
/**
 * Description of ChequeAPI
 *
 * @author meza
 */
class ChequeAPI extends EntityAPI {
    const API_ACTION = 'cheque';
    const GET_ENCARTERA = 'encartera';
    const PUT_COBRADO = 'cobrado';
  
    public function __construct() {
        
	$this->db = new ChequeDB();
        $this->fields = [];
        array_push($this->fields, 
            'origen',
            'idbanco',
            'tenedororiginal', // es el que entrega el cheque puede coincidir o no con el librador
            'librador', // es el que emite el cheque y debe cambiar
            'numero',
            'fecemision',
            'fecvencimiento',
            'monto',
            'depositado',
            'cobrado');
    }
        
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $encartera = isset($id) ? $id === self::GET_ENCARTERA : false;
        
        if ($encartera) {
            $response = $this->db->getEnCartera();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        
        $r = $this->db->insert(
                $obj->origen, $obj->idbanco, $obj->tenedororiginal,
                $obj->librador, $obj->numero, $obj->fecemision, 
                $obj->fecvencimiento, $obj->monto, $obj->depositado, 
                $obj->cobrado);
        if($r) {$this->response(200,"success",$r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        $isCobrado = isset($id) ? $id === self::PUT_COBRADO : false;
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }        
        $r=null;
        
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->update($id, 
                $obj->origen, $obj->idbanco, $obj->tenedororiginal,
                $obj->librador, $obj->numero, $obj->fecemision, 
                $obj->fecvencimiento, $obj->monto, $obj->depositado, 
                $obj->cobrado);

        if($r) { $this->response(200,"success", $r); }
        else { $this->response(204,"success","Record not updated");}
    }
}