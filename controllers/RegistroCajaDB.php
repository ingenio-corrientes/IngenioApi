<?php
/**
 * Description of RegistroCajaDB
 *
 * @author meza
 */
class RegistroCajaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'registroscaja';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT * FROM ". self::TABLE;
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getRegistro(){
        $query = "SELECT u.*
            FROM registroscaja u
            ORDER BY u.id DESC LIMIT 1;";
//        $query = "SELECT ;u.*, 
//                IFNULL(
//                    (SELECT a.efectivoarqueo
//                    FROM registroscaja a 
//                    WHERE a.id = u.id - 1), 0) AS efectivoanterior
//            FROM registroscaja u 
//            ORDER BY u.id DESC LIMIT 1";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getChequesEnCartera(){
        $query = "SELECT c.id, c.idbanco, b.banco, c.numero, c.fecemision, 
                c.fecvencimiento, c.monto, c.depositado, c.fecultmodif
            FROM cheques c 
            LEFT JOIN bancos b ON c.idbanco = b.id
            LEFT JOIN recibositems r ON r.idcheque = c.id
            WHERE c.depositado = 0;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getTotales(){
        $query = "SELECT (IFNULL(q1.efectivo, 0) - IFNULL(q3.total, 0)) AS efectivo, IFNULL(q2.total, 0) AS total, IFNULL(q3.total, 0) AS totalretiros
            FROM
                (SELECT 1 AS uni, (IFNULL(t3.efectivo,  0)) + (IFNULL(t1.efectivo,  0) - IFNULL(t2.efectivo, 0)) AS efectivo
                FROM
                    (SELECT IFNULL(SUM(r.monto), 0) AS efectivo
                    FROM formascobro c
                    LEFT JOIN recibositems r ON r.idformacobro = c.id
                    LEFT JOIN recibos b ON b.id = r.idrecibo
                    WHERE c.bancario = 0 AND c.cheque = 0 
                       AND r.idregistrocaja = 0 AND b.fecemision IS NOT NULL) t1 -- total de cobros en efectivo
                JOIN
                    (SELECT IFNULL(SUM(g.monto), 0) AS efectivo
                    FROM formaspago p
                    LEFT JOIN gastositems g ON g.idformapago = p.id
                    LEFT JOIN gastos t ON t.id = g.idgasto
                    WHERE p.bancario = 0 AND p.cheque = 0 
                        AND g.idregistrocaja = 0 AND t.fecemision IS NOT NULL) t2 -- total de gastos en efectivo
                JOIN
                    (SELECT IFNULL(rc.efectivoinicio, 0) AS efectivo
                    FROM registroscaja rc
                    ORDER BY rc.id DESC LIMIT 1) t3 -- efectivo al inicio del día
                ) q1 -- total de efectivo
            JOIN
                (SELECT 1 AS uni, (ef.efectivoinicio + IFNULL(fc.total, 0) - IFNULL(fp.total, 0) - IFNULL(re.total, 0)) AS total
                FROM
                    (SELECT SUM(r.monto) AS total
                    FROM formascobro c
                    LEFT JOIN recibositems r ON r.idformacobro = c.id
                    LEFT JOIN recibos b ON b.id = r.idrecibo
                    WHERE r.idregistrocaja = 0 AND b.fecemision IS NOT NULL) fc -- total de cobros
                JOIN
                    (SELECT SUM(g.monto) AS total
                    FROM formaspago p
                    LEFT JOIN gastositems g ON g.idformapago = p.id
                    LEFT JOIN gastos t ON t.id = g.idgasto
                    WHERE g.idregistrocaja = 0 AND t.fecemision IS NOT NULL) fp -- total de gastos
                JOIN
                    (SELECT SUM(v.monto) AS total
                    FROM retiroefectivos v 
                    WHERE v.idregistrocaja = 0) re -- total retiros de efectivo
                JOIN
                    (SELECT rc.efectivoinicio
                    FROM registroscaja rc
                    ORDER BY rc.id DESC LIMIT 1) ef -- efectivo al inicio
                ) q2 -- total de movimientos
            JOIN
                (SELECT SUM(v.monto) AS total
                FROM retiroefectivos v
                WHERE v.idregistrocaja = 0 -- total de retiros
                ) q3;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
//    public function getTotales(){
//        $query = "SELECT IFNULL(q1.efectivo, 0) AS efectivo, IFNULL(q2.total, 0) AS total, IFNULL(q3.total, 0) AS totalretiros
//            FROM
//                (SELECT 1 AS uni, (IFNULL(t1.efectivo,  0) - IFNULL(t2.efectivo, 0)) AS efectivo
//                FROM
//                    (SELECT SUM(r.monto) AS efectivo
//                    FROM formascobro c
//                    LEFT JOIN recibositems r ON r.idformacobro = c.id
//                    LEFT JOIN recibos b ON b.id = r.idrecibo
//                    JOIN
//                        (SELECT 
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecinicio, NOW())) != 0 THEN IFNULL(rc.fecinicio, NOW()) ELSE NOW() END AS fecinicio,
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecarqueo, NOW())) != 0 THEN IFNULL(rc.fecarqueo, NOW()) ELSE NOW() END AS fecarqueo
//                        FROM registroscaja rc
//                        ORDER BY rc.id DESC LIMIT 1) rg
//                    WHERE c.bancario = 0 AND c.cheque = 0 
//                        AND (b.fecha BETWEEN rg.fecinicio AND rg.fecarqueo)) t1
//                JOIN
//                    (SELECT SUM(g.monto) AS efectivo
//                    FROM formaspago p
//                    LEFT JOIN gastositems g ON g.idformapago = p.id
//                    LEFT JOIN gastos t ON t.id = g.idgasto
//                    JOIN
//                        (SELECT
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecinicio, NOW())) != 0 THEN IFNULL(rc.fecinicio, NOW()) ELSE NOW() END AS fecinicio,
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecarqueo, NOW())) != 0 THEN IFNULL(rc.fecarqueo, NOW()) ELSE NOW() END AS fecarqueo
//                        FROM registroscaja rc
//                        ORDER BY rc.id DESC LIMIT 1) rg
//                    WHERE p.bancario = 0 AND p.cheque = 0 
//                        AND (t.fecha  BETWEEN rg.fecinicio AND rg.fecarqueo)) t2
//                ) q1
//            JOIN
//                (SELECT 1 AS uni, (ef.efectivoinicio + IFNULL(fc.total, 0) - IFNULL(fp.total, 0) - IFNULL(re.total, 0)) AS total
//                FROM
//                    (SELECT SUM(r.monto) AS total
//                    FROM formascobro c
//                    LEFT JOIN recibositems r ON r.idformacobro = c.id
//                    LEFT JOIN recibos b ON b.id = r.idrecibo
//                    JOIN
//                        (SELECT
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecinicio, NOW())) != 0 THEN IFNULL(rc.fecinicio, NOW()) ELSE NOW() END AS fecinicio,
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecarqueo, NOW())) != 0 THEN IFNULL(rc.fecarqueo, NOW()) ELSE NOW() END AS fecarqueo
//                        FROM registroscaja rc
//                        ORDER BY rc.id DESC LIMIT 1) rg
//                    WHERE b.fecha BETWEEN rg.fecinicio AND rg.fecarqueo) fc
//                JOIN
//                    (SELECT SUM(g.monto) AS total
//                    FROM formaspago p
//                    LEFT JOIN gastositems g ON g.idformapago = p.id
//                    LEFT JOIN gastos t ON t.id = g.idgasto
//                    JOIN
//                        (SELECT
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecinicio, NOW())) != 0 THEN IFNULL(rc.fecinicio, NOW()) ELSE NOW() END AS fecinicio,
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecarqueo, NOW())) != 0 THEN IFNULL(rc.fecarqueo, NOW()) ELSE NOW() END AS fecarqueo
//                        FROM registroscaja rc
//                        ORDER BY rc.id DESC LIMIT 1) rg
//                    WHERE t.fecha BETWEEN  rg.fecinicio AND rg.fecarqueo) fp
//                JOIN
//                    (SELECT SUM(v.monto) AS total
//                    FROM retiroefectivos v 
//                    JOIN
//                        (SELECT 
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecinicio, NOW())) != 0 THEN IFNULL(rc.fecinicio, NOW()) ELSE NOW() END AS fecinicio,
//                            CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecarqueo, NOW())) != 0 THEN IFNULL(rc.fecarqueo, NOW()) ELSE NOW() END AS fecarqueo
//                        FROM registroscaja rc
//                        ORDER BY rc.id DESC LIMIT 1) rg
//                    WHERE v.fecha BETWEEN rg.fecinicio AND rg.fecarqueo) re
//                JOIN
//                    (SELECT rc.efectivoinicio
//                    FROM registroscaja rc
//                    ORDER BY rc.id DESC LIMIT 1) ef
//                ) q2
//            JOIN
//                (SELECT SUM(v.monto) AS total
//                FROM retiroefectivos v 
//                JOIN
//                    (SELECT 
//                        CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecinicio, NOW())) != 0 THEN IFNULL(rc.fecinicio, NOW()) ELSE NOW() END AS fecinicio,
//                        CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecarqueo, NOW())) != 0 THEN IFNULL(rc.fecarqueo, NOW()) ELSE NOW() END AS fecarqueo
//                    FROM registroscaja rc
//                    ORDER BY rc.id DESC LIMIT 1) rg
//                WHERE v.fecha BETWEEN rg.fecinicio AND rg.fecarqueo) q3;";
//        var_dump($query);
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
//    }
    
    public function getFormasCobroPago() {
        $query = "SELECT c.formacobro AS forma, SUM(i.monto) AS total, 0 AS tipo
            FROM recibositems i
            LEFT JOIN recibos r ON r.id = i.idrecibo
            LEFT JOIN formascobro c ON c.id = i.idformacobro
            WHERE i.idregistrocaja = 0 AND r.fecemision IS NOT NULL
            GROUP BY c.id
            UNION
            SELECT p.formapago AS forma, SUM(i.monto) AS total, 1 AS tipo
            FROM gastositems i
            LEFT JOIN gastos g ON g.id = i.idgasto
            LEFT JOIN formaspago p ON p.id = i.idformapago
            WHERE i.idregistrocaja = 0 AND g.fecemision IS NOT NULL
            GROUP BY p.id;";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
  
    public function getRetirosEfectivo() {
        $query = "SELECT r.id,
                CONCAT(u.apellido, ', ', u.nombre) AS responsable, r.fecha,
                r.concepto, r.monto
            FROM retiroefectivos r
            LEFT JOIN usuarios u ON u.id = r.idresponsable
            WHERE r.idregistrocaja = 0;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
//    public function getRetirosEfectivo() {
//        $query = "SELECT r.id,
//                CONCAT(u.apellido, ', ', u.nombre) AS responsable, r.fecha,
//                r.concepto, r.monto
//            FROM retiroefectivos r
//            LEFT JOIN usuarios u ON u.id = r.idresponsable
//            JOIN
//                (SELECT id,  
//                    CAST(CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecinicio, NOW())) != 0 THEN IFNULL(rc.fecinicio, NOW()) ELSE NOW() END AS DATE) AS fecinicio,
//                    CAST(CASE WHEN UNIX_TIMESTAMP(IFNULL(rc.fecarqueo, NOW())) != 0 THEN IFNULL(rc.fecarqueo, NOW()) ELSE NOW() END AS DATE)AS fecarqueo
//                FROM registroscaja rc
//                ORDER BY rc.id DESC LIMIT 1) rg
//            WHERE r.fecha BETWEEN rg.fecinicio AND rg.fecarqueo;";
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
//    }
    
    public function getInformeGeneral($fecDesde, $fecHasta){
        $query = "SELECT IFNULL(ing.totalingreso, 0) AS totalingreso, 
                IFNULL(egr.totalegreso, 0) AS totalegreso
            FROM
                (SELECT SUM(i.monto) AS totalingreso
                FROM recibositems i
                LEFT JOIN recibos r ON r.id = i.idrecibo
                LEFT JOIN formascobro c ON c.id = i.idformacobro
                WHERE r.fecha BETWEEN '$fecDesde' AND '$fecHasta') ing
            JOIN
                (SELECT SUM(i.monto) AS totalegreso
                FROM gastositems i
                LEFT JOIN gastos g ON g.id = i.idgasto
                LEFT JOIN formaspago p ON p.id = i.idformapago
                WHERE g.fecha BETWEEN '$fecDesde' AND '$fecHasta') egr";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getInformeCobroPago($fecDesde, $fecHasta) {
        $query = "SELECT c.formacobro AS forma, SUM(i.monto) AS total, 0 AS tipo
            FROM recibositems i
            LEFT JOIN recibos r ON r.id = i.idrecibo
            LEFT JOIN formascobro c ON c.id = i.idformacobro
            WHERE r.fecha BETWEEN '$fecDesde' AND '$fecHasta'
            GROUP BY c.id
            UNION
            SELECT p.formapago AS forma, SUM(i.monto) AS total, 1 AS tipo
            FROM gastositems i
            LEFT JOIN gastos g ON g.id = i.idgasto
            LEFT JOIN formaspago p ON p.id = i.idformapago
            WHERE g.fecha BETWEEN '$fecDesde' AND '$fecHasta'
            GROUP BY p.id;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($fecinicio, $efectivoinicio, 
            $fecarqueo, $efectivoarqueo){
        $now = (new DateTime())->format('Y-m-d H:i:s');
        
        if (strlen($fecarqueo) > 0) {
            $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
            $stmt->execute();
            
            $query = "CALL reg_arqueo('$now', $efectivoarqueo, '$now', @p_oresult);";
    //        var_dump($query);
            $r = $this->mysqli->query($query);

            $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
            $row = $r->fetch_assoc();
            return $row['p_oresult'];
        } else {
            $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
            $stmt->execute();
            
            $query = "CALL reg_inicio_dia('$now', $efectivoinicio, '$now', @p_oresult);";
//            var_dump($query);
            $r = $this->mysqli->query($query);

            $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
            $row = $r->fetch_assoc();
            
            return $row['p_oresult'];
        }
        return -1;
    }
	
//    public function insert($fecinicio, $efectivoinicio, 
//            $fecarqueo, $efectivoarqueo){
//                
//        if (strlen($fecarqueo) > 0) {
//            $query = "INSERT INTO registroscaja 
//                    (fecinicio, efectivoinicio, 
//                    fecarqueo, efectivoarqueo, fecultmodif) 
//                VALUES 
//                    ('$fecinicio', $efectivoinicio, 
//                    '$fecarqueo', $efectivoarqueo, NOW());";
//        } else {
//            $query = "INSERT INTO registroscaja 
//                    (fecinicio, efectivoinicio, 
//                    efectivoarqueo, fecultmodif) 
//                VALUES 
//                    ('$fecinicio', $efectivoinicio, 
//                    $efectivoarqueo, NOW());";
//        }
////         var_dump($query);
//        $stmt = $this->mysqli->prepare($query );
//        $r = $stmt->execute();
//        $stmt->close();
//        $lastid = $this->mysqli->insert_id;
////         var_dump($lastid);
////         return true;
//        return $lastid;
//    }
    
    public function update($id=-1, 
            $fecinicio='', $efectivoinicio='', 
            $fecarqueo='', $efectivoarqueo=0) {
        $now = (new DateTime())->format('Y-m-d H:i:s');
        
        if (strlen($fecarqueo) > 0) {
            $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
            $stmt->execute();
            
            $query = "CALL reg_arqueo('$now', $efectivoarqueo, '$now', @p_oresult);";
    //        var_dump($query);
            $r = $this->mysqli->query($query);

            $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
            $row = $r->fetch_assoc();
            return $row['p_oresult'];
        }
        return false;
//        $query = "UPDATE registroscaja SET 
//                fecinicio='$fecinicio', efectivoinicio=$efectivoinicio,
//                fecarqueo='$fecarqueo', efectivoarqueo=$efectivoarqueo, 
//                fecultmodif=NOW()
//            WHERE id = $id;";
////        var_dump($query);
//        if($this->checkIntID(self::TABLE, $id)){
//            $stmt = $this->mysqli->prepare($query);
//            $r = $stmt->execute(); 
//            $stmt->close();
//            return $r;
//        }
//        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }	
}
