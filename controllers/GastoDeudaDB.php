<?php
/**
 * Description of GastoDeudaDB
 *
 * @author meza
 */
class GastoDeudaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'gastosdeudas';
    
    public function getById($id=''){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id='?';");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT d.id, d.idgasto, d.idcompra, d.montoacancelar, 
            c.idtipocomprobante, t.tipocomprobante, c.numero, 
            c.fecemision, c.subtotalneto, c.total, c.saldo
            FROM gastosdeudas d
            LEFT JOIN compra c ON c.id = d.idcompra
            LEFT JOIN tiposcomprobantes t ON t.id = c.idtipocomprobante";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getListByIDGasto($idgasto=-1) {
        $query = "SELECT d.id, d.idgasto, d.idcompra, d.montoacancelar, 
            c.idtipocomprobante, t.tipocomprobante, c.numero, 
            c.fecemision, c.subtotal, c.total, c.saldo
            FROM gastosdeudas d
            LEFT JOIN compras c ON c.id = d.idcompra
            LEFT JOIN tiposcomprobantes t ON t.id = c.idtipocomprobante
            WHERE d.idgasto = $idgasto";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idgasto=-1, $idcompra='', $montoacancelar=0){
        $id = $this->gen_uuid();
        $query = "INSERT INTO gastosdeudas
                (id, idgasto, idcompra, montoacancelar, fecultmodif) 
            VALUES 
                ('$id', $idgasto, '$idcompra', $montoacancelar, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    public function update($id, $idgasto, $idcompra, $montoacancelar) {
        $query = "UPDATE gastosdeudas SET 
                idgasto = $idgasto, idcompra = '$idcompra', montoacancelar = $montoacancelar,
                fecultmodif= NOW() 
                WHERE id = '$id';";
        if($this->checkStringID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function reloadDeudas($idgasto) {
//        $idgastoid = $this->gen_uuid();
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL sp_recargar_deudas($idgasto, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        
//        var_dump($row['p_oresult']);
//        return true;
        return $row['p_oresult'];
    }
	
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}
