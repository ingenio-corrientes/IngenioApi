<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdCarteleraAPI
 *
 * @author meza
 */
class AdCarteleraAPI extends EntityAPI {
    const API_ACTION = 'adcartelera';
  
    public function __construct() {
        
	$this->db = new AdCarteleraDB();
        $this->fields = [];
        array_push($this->fields, 
            'carteleria',
            'anchomin',
            'altomin',
            'unidad');
    }
    
    function processPost() {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->carteleria) AND isset($obj->anchomin) 
                    AND isset($obj->altomin) AND isset($obj->unidad) AND isset($obj->fecultmodif)) {
                $r = $this->db->insert($obj->carteleria, $obj->anchomin, 
                        $obj->altomin, $obj->unidad, $obj->fecultmodif);
                if($r)
                    $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        }
        else
        {
            $this->response(400);
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(isset($id)){
            $obj = json_decode( file_get_contents('php://input') );   
            $objArr = (array)$obj;
            if (empty($objArr)){                        
                $this->response(422,"error","Nothing to add. Check json");
                exit();
            }
            if(!$this->checkFields($obj)){
                $this->response(422,"error","The property is not defined");                        
                exit();
            }
            $r = $this->db->update($id, $obj->carteleria, $obj->anchomin, 
                    $obj->altomin, $obj->unidad);
            if($r) {
                $this->response(200,"success","Record updated");
            } else {
                $this->response(304,"success","Record not updated");                             
            }
            exit();
        }
        $this->response(400);
    }
}