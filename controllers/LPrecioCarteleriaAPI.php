<?php
/**
 * Description of LPrecioCarteleriaAPI
 *
 * @author meza
 */
class LPrecioCarteleriaAPI extends EntityAPI {
    const GET_LISTBYIDLISTA = 'byidlista';
    const PUT_UPDATELISTA = 'updatelista';
    const API_ACTION = 'lpreciocarteleria';

    public function __construct() {
        $this->db = new LPrecioCarteleriaDB();
        $this->fields = [];
        array_push($this->fields, 
                'idlistaprecio',
                'idsubtiposustrato',
                'precio');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdLista= isset($id) ? ($id === self::GET_LISTBYIDLISTA) : false;
        if($isByIdLista) {
            $idlista = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdLista($idlista);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idlistaprecio, $obj->idsubtiposustrato, $obj->precio);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id){
            $this->response(400, "error", "The id not defined");
            exit;
        }
        
        $isUpdateLista = isset($id) ? $id === self::PUT_UPDATELISTA : false;
        if($isUpdateLista) {
            $this->processUpdateLista();
        } else {
            $this->processPutGral($id);
        }
    }
    
    function processUpdateLista() {
        $idlista = filter_input(INPUT_GET, 'fld1');
        $r = $this->db->updateLista($idlista);
//        var_dump($r);
        if($r === 0) {
            $this->response(200,"success",$r);
        } else {
            $this->response(304,"error", $r);
        }
    }
    
    function processPutGral($id=-1) {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idlistaprecio, $obj->idsubtiposustrato, $obj->precio);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}
