<?php
/**
 * Description of ModuloXRolDB
 *
 * @author meza
 */
class ModuloXRolDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'modulosxroles';
    
    public function getList(){
        $query = "SELECT i.id, i.idrol, r.rol, i.idmodulo, m.modulo, "
                . "i.ver, i.editar, i.eliminar "
                . "FROM modulosxroles i "
                . "LEFT JOIN roles r ON r.id = i.idrol "
                . "LEFT JOIN modulos m ON m.id = i.idmodulo;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idrol, r.rol, i.idmodulo, m.modulo, "
                . "i.ver, i.editar, i.eliminar "
                . "FROM modulosxroles i "
                . "LEFT JOIN roles r ON r.id = i.idrol "
                . "LEFT JOIN modulos m ON m.id = i.idmodulo "
                . "WHERE i.id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getByIdRol($idrol=0){
        $query = "SELECT i.id, i.idrol, r.rol, i.idmodulo, m.modulo, "
                . "i.ver, i.editar, i.eliminar "
                . "FROM modulosxroles i "
                . "LEFT JOIN roles r ON r.id = i.idrol "
                . "LEFT JOIN modulos m ON m.id = i.idmodulo "
                . "WHERE i.idrol = $idrol;";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getByIdUsuarioIdModulo($idusuario=0, $idmodulo=0){
        $query = "SELECT i.id, i.idrol, r.rol, i.idmodulo, m.modulo, 
                i.ver, i.editar, i.eliminar 
            FROM modulosxroles i 
            LEFT JOIN roles r ON r.id = i.idrol 
            LEFT JOIN usuarios u ON u.idrol = r.id
            LEFT JOIN modulos m ON m.id = i.idmodulo 
            WHERE i.idmodulo = $idmodulo AND u.id = $idusuario;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert(
            $idrol=-1, $idmodulo=-1, 
            $ver=0, $editar=0, 
            $eliminar=0){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " ("
                . "idrol, idmodulo, "
                . "idver, editar, "
                . "eliminar) "
                . "VALUES ("
                . "$idrol, $idmodulo, "
                . "$ver, $editar,"
                . "$eliminar);");
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1,
            $idrol=-1, $idmodulo=-1, 
            $ver=0, $editar=0, 
            $eliminar=0) {
        $query = "UPDATE " . self::TABLE . " SET "
                . "idrol= $idrol, idmodulo= $idmodulo, "
                . "ver= $ver, editar= $editar, "
                . "eliminar= $eliminar "
                . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}