<?php
/**
 * Description of GastoItemItemDB
 *
 * @author meza
 */
class GastoItemDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'gastositems';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT i.id, i.idgasto, i.idformapago, f.formapago, i.idbanco, 
                b.banco, i.origendestino, i.numero, i.propio, 
                i.fecha, i.fecvencimiento, i.monto, i.fecultmodif 
            FROM gastositems i 
            LEFT JOIN formaspago f ON f.id = i.idformapago 
            LEFT JOIN bancos b ON b.id = i.idbanco;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdGasto($idgasto=0){
        $query = "SELECT i.id, i.idgasto, i.idformapago, f.formapago, i.idbanco, 
                IFNULL(b.banco, '') AS banco, i.origendestino, i.numero, i.propio, 
                i.fecha, i.fecvencimiento, i.monto, i.idcheque, i.fecultmodif 
            FROM gastositems i 
            LEFT JOIN formaspago f ON f.id = i.idformapago 
            LEFT JOIN bancos b ON b.id = i.idbanco 
            WHERE i.idgasto = $idgasto;";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert(
            $idgasto=-1, $idformapago=-1, $idbanco='', 
            $origendestino='', $idcheque=0, $numero='', 
            $fecha='', $propio=0, $fecvencimiento='', 
            $monto=-1){
        
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_gastoitem(0, -1,
                $idgasto, $idformapago, $idbanco, 
                '$origendestino', $idcheque, '$numero',
                '$fecha', '$fecvencimiento', $propio,
                $monto, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idgasto=-1, $idformapago=-1, $idbanco='', 
            $origendestino='', $idcheque=0, $numero='', 
            $fecha='', $propio=0, $fecvencimiento='',
            $monto=-1) {
        
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_gastoitem(1, $id,
                $idgasto, $idformapago, $idbanco,
                '$origendestino', $idcheque, '$numero',
                '$fecha', '$fecvencimiento', $propio,
                $monto, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_gastoitem(2, $id,
            -1, -1, -1,
            '', 0, '', 
            '', '', 0,
            -1, @p_oresult);";
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
}