<?php
/**
 * Description of TipoAdicionalAPI
 *
 * @author meza
 */
class TipoAdicionalAPI extends EntityAPI {
    const GET_LISTBYIDSERVICIO = 'byidservicio';
    const API_ACTION = 'tipoadicional';
    
    public function __construct() {
        $this->db = new TipoAdicionalDB();
        $this->fields = [];
        array_push($this->fields, 
                'idtiposervicio', 
                'tipoadicional', 
                'anchomin', 
                'altomin', 
                'unidad');
    }
	
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdServicio = strpos($id, self::GET_LISTBYIDSERVICIO);
        if($isByIdServicio !== false) {
            $idservicio = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdServicio($idservicio);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idtiposervicio, $obj->tipoadicional, $obj->anchomin, 
                $obj->altomin, $obj->unidad);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idtiposervicio, $obj->tipoadicional, $obj->anchomin, 
                $obj->altomin, $obj->unidad);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}
