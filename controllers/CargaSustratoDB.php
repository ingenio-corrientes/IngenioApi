<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CargaSustratoDB
 *
 * @author meza
 */
class CargaSustratoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'cargasustratos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT id, sustrato, fecultmodif "
                . "FROM cargasustratos ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($sustrato='', $fecultmodif=''){
		$query= "INSERT INTO " . self::TABLE . " (sustrato, fecultmodif) "
                . "VALUES ('$sustrato', NOW());";
		//var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, $sustrato='', $fecultmodif='') {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET sustrato= '$sustrato', fecultmodif= NOW() "
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}