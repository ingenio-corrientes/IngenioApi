<?php
/**
 * Description of SubTipoSustratoAPI
 *
 * @author meza
 */
class SubTipoSustratoAPI extends EntityAPI {
    const API_ACTION = 'subtiposustrato';
    
    public function __construct() {
        $this->db = new SubTipoSustratoDB();
        $this->fields = [];
        array_push($this->fields, 
                'idsustrato',
                'subtiposustrato',
                'anchomin',
                'altomin',
                'anchomax',
                'altomax');
    }
    
    function processGet(){         
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idsustrato, $obj->subtiposustrato,
                $obj->anchomin, $obj->altomin, 
                $obj->anchomax, $obj->altomax);
        if($r) {$this->response(200,"success",$r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idsustrato, $obj->subtiposustrato, $obj->anchomin, 
                $obj->altomin, $obj->anchomax, $obj->altomax);
        if($r) { $this->response(200,"success",$r); }
        else { $this->response(204,"success","Record not updated");}
    }
}
