<?php
/**
 * Description of ListaPrecioAPI
 *
 * @author meza
 */
class ListaPrecioAPI extends EntityAPI {
    const GET_LISTASBASE = 'getlistasbase';
    const GET_BYIDPRESUPUESTO = 'getbyidpresupuesto';
    const PUT_APPLY = 'apply';
    const API_ACTION = 'listaprecio';
    
    const APLICAR_PORCENTAJE = 1;
    const APLICAR_MONTO = 1;

    public function __construct() {
        $this->db = new ListaPrecioDB();
        $this->fields = [];
        array_push($this->fields, 
                'idtipocliente',
                'idtiposervicio',
                'idlistabase',
                'listaprecio',
                'desyreca',
                'monto',
                'doblefaz',
                'color');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isGetListasBase = isset($id) ? ($id === self::GET_LISTASBASE) : false;
        $isGetBYIdPresupuesto = isset($id) ? ($id === self::GET_BYIDPRESUPUESTO) : false;
        
        if($isGetListasBase) {
            $tiposervicio = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getListasBase($tiposervicio);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isGetBYIdPresupuesto){
            $idpresupuesto = filter_input(INPUT_GET, 'fld1');
            $tiposervicio = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getByIdPresupuesto($idpresupuesto, $tiposervicio);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idtipocliente, $obj->idtiposervicio, $obj->idlistabase, 
                $obj->listaprecio, $obj->desyreca, $obj->monto,
                $obj->doblefaz, $obj->color);
        if($r) {$this->response(200,"success",$r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        $id = filter_input(INPUT_GET, 'id');
        $isGetListasBase = isset($id) ? ($id === self::PUT_APPLY) : false;
        
        if($isGetListasBase) {
            $id = filter_input(INPUT_GET, 'fld1');
            $tipo = filter_input(INPUT_GET, 'fld2');
            $r = $this->db->apply($obj->idtiposervicio, 
                $obj->id, $obj->desyreca, $obj->monto);
            if($r) { $this->response(200,"success",$id); }
            else { $this->response(204,"success","Record not updated");}
            exit;
        }
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->update($id,
            $obj->idtipocliente, $obj->idtiposervicio, $obj->idlistabase, 
            $obj->listaprecio, $obj->desyreca, $obj->monto,
            $obj->doblefaz, $obj->color);
        if($r) { $this->response(200,"success",$id); }
        else { $this->response(204,"success","Record not updated");}
    }
}
