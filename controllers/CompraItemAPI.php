<?php
/**
 * Description of CompraItemAPI
 *
 * @author meza
 */
class CompraItemAPI extends EntityAPI {
    const GET_ITEMSBYIDCOMPRA = 'itemsbyidcompra';
    const API_ACTION = 'compraitem';
  
    public function __construct() {
	$this->db = new CompraItemDB();
        $this->fields = [];
        array_push($this->fields, 
                'idcompra',
                'descripcion', 
                'precio',
                'cantidad');
    }
         
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isItemsByIdCompra = isset($id) ? $id === self::GET_ITEMSBYIDCOMPRA : false;
        
        if ($isItemsByIdCompra) {
            $idcompra = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByItemsIdCompra($idcompra);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id) {
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idcompra, $obj->descripcion, $obj->precio, 
                $obj->cantidad);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idcompra, $obj->descripcion, $obj->precio, 
                $obj->cantidad);
        
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}
