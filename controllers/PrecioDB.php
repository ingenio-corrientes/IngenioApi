<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrecioDB
 *
 * @author meza
 */
class PrecioDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'precios';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query =
                "SELECT p.id, p.precio, p.idtiposerv, t.servicio, "
                . "p.tipoclient, p.desyreca, p.monto, p.idlistabase, p.fecultmodif, 2 AS descripcion " //harcoder
                . "FROM precios p "
                . "LEFT JOIN tiposservicios t ON p.idtiposerv = t.id ";
    //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($precio='', $idtiposerv=-1, 
			$tipoclient='', $desyreca=-1, 
			$monto=-1, $idlistabase=-1, $fecultmodif=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " "
                . "(precio, idtiposerv, "
                . "tipoclient, desyreca, "
                . "monto, idlistabase, fecultmodif) "
                . "VALUES "
                . "('$precio', $idtiposerv, "
                . "'$tipoclient', $desyreca, "
                . "$monto, $idlistabase, NOW());");
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, 
			$precio='', $idtiposerv=-1, 
			$tipoclient='', $desyreca=-1, 
			$monto=-1, $idlistabase=-1, $fecultmodif='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET "
                    . "precio= '$precio', idtiposerv= $idtiposerv, "
                    . "tipoclient= '$tipoclient', desyreca= $desyreca, "
                    . "monto= $monto, idlistabase= $idlistabase, fecultmodif= NOW() "
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}