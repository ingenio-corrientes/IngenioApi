<?php
/**
 * Description of GastoDeudaAPI
 *
 * @author meza
 */
class GastoDeudaAPI extends EntityAPI {
    const GET_LISTBYIDGASTO = 'listbyidgasto';
    const PUT_RELOAD_DEUDAS = 'reloaddeudas';
    const API_ACTION = 'gastodeuda';

    public function __construct() {
        $this->db = new GastoDeudaDB();
        $this->fields = [];
        array_push($this->fields, 
                'idgasto',
                'idcompra',
                'montoacancelar');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isListByIDGasto = isset($id) ? $id === self::GET_LISTBYIDGASTO : false;
        if($isListByIDGasto) {
            $idfld = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getListByIDGasto($idfld);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idgasto, $obj->idcompra, $obj->montoacancelar);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        $id = filter_input(INPUT_GET, 'id');
        $isReloadDeudas = isset($id) ? ($id === self::PUT_RELOAD_DEUDAS) : false;
        
        if($isReloadDeudas) {
            $id = filter_input(INPUT_GET, 'fld1');
            $r = $this->db->reloadDeudas($id);
            if($r) { $this->response(200,"success",$id); }
            else { $this->response(204,"success","Record not updated");}
            exit;
        } 
       if(!$id) {
           $this->response(422,"error","Id no enviado.");
            exit;
       }
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idgasto, $obj->idcompra, $obj->montoacancelar);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}
