<?php
/**
 * Description of RetiroEfectivoDB
 *
 * @author meza
 */
class RetiroEfectivoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'retiroefectivos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT r.id, r.idresponsable, "
                . "CONCAT(u.apellido, ', ', u.nombre)as responsable, r.fecha, "
                . "r.concepto, r.monto "
                . "FROM retiroefectivos r "
                . "LEFT JOIN usuarios u ON u.id = r.idresponsable;");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idresponsable=-1, $fecha='', $concepto='', $monto=0){
        $query = "INSERT INTO " . self::TABLE . " (idresponsable, fecha, concepto, monto) "
                . "VALUES ($idresponsable, '$fecha', '$concepto', $monto);";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function update($id=-1, 
            $idresponsable=-1, $fecha='', $concepto='',
            $monto=0) {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET 
                        idresponsable=$idresponsable, fecha='$fecha', concepto='$concepto',
                        monto = $monto
                    WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
