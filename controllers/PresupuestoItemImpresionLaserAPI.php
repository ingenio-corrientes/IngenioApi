<?php
/**
 * Description of PresupuestoItemImpresionLaserAPI
 *
 * @author meza
 */
class PresupuestoItemImpresionLaserAPI extends EntityAPI {
    const GET_BYIDPRESUPUESTO = 'byidpresupuesto';
    const API_ACTION = 'presupuestoitemimpresionlaser';

    public function __construct() {
        $this->db = new PresupuestoItemImpresionLaserDB();
        $this->fields = [];
        array_push($this->fields,
                'idpresupuesto', 
                'idgramaje', 
                'idtamano',
                'descripcion', 
                'color',
                'doblefaz',
                'cantidad',
                'precio');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdPResupuesto = ($id == self::GET_BYIDPRESUPUESTO);
        
        if($isByIdPResupuesto){
            $fld1 = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdPresupuesto($fld1);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idpresupuesto, $obj->idgramaje, $obj->idtamano,
                $obj->descripcion, $obj->color, $obj->doblefaz,
                $obj->cantidad, $obj->precio);
        if($r) {$this->response(200,"success",$r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idpresupuesto, $obj->idgramaje, $obj->idtamano, 
                $obj->descripcion, $obj->color, $obj->doblefaz,
                $obj->cantidad, $obj->precio);
        if($r) { $this->response(200,"success",$id); }
        else { $this->response(204,"success","Record not updated");}
    }
}