<?php
/**
 * Description of ReciboItemAPI
 *
 * @author meza
 */
class ReciboItemAPI extends EntityAPI {
    const GET_LISTBYIDRECIBO = 'byidrecibo';
    const API_ACTION = 'reciboitem';
    
    public function __construct() {
        $this->db = new ReciboItemDB();
        $this->fields = [];
        array_push($this->fields, 
                'idrecibo', 
                'idformacobro', 
                'idbanco', 
                'origendestino', // siempre va a ser el tenedor original del cheque, es decir el que entrega el cheque
                'librador', // es el que emite el cheque que puede o no coincidir con el tenedor original.
                'numero', 
                'fecha',
                'fecvencimiento',
                'monto');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdRecibo = strpos($id, self::GET_LISTBYIDRECIBO);
        
        if($isByIdRecibo !== false) {
            $idrecibo = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdRecibo($idrecibo);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id !== false){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
   
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idrecibo, $obj->idformacobro, $obj->idbanco, 
                $obj->origendestino, $obj->librador, $obj->numero, 
                $obj->fecha, $obj->fecvencimiento, $obj->monto);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idrecibo, $obj->idformacobro, $obj->idbanco, 
                $obj->origendestino, $obj->librador, $obj->numero,
                $obj->fecha, $obj->fecvencimiento, $obj->monto);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}