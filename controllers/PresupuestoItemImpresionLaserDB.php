<?php
/**
 * Description of PresupuestoItemImpresionLaserDB
 *
 * @author meza
 */
class PresupuestoItemImpresionLaserDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestositemsimpresionlaser';
    
    public function getById($id=0){
        $query = "SELECT * FROM presupuestositemsimpresionlaser WHERE id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT i.id, i.idpresupuesto, i.idgramaje, 
                i.idtamano, i.color, i.descripcion, 
                i.cantidad, i.precio, i.fecultmodif 
            FROM presupuestositemsimpresionlaser i 
            LEFT JOIN gramajes g ON i.idgramaje = g.id
            LEFT JOIN tamanos n ON i.idtamano = n.id;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPresupuesto($idpresupuesto) {
        $query = "SELECT i.id, i.idpresupuesto, i.idgramaje, 
                i.idtamano, i.descripcion, i.color, i.doblefaz, 
                i.cantidad, i.precio, i.fecultmodif 
            FROM presupuestositemsimpresionlaser i 
            LEFT JOIN gramajes g ON i.idgramaje = g.id
            LEFT JOIN tamanos n ON i.idtamano = n.id
            WHERE i.idpresupuesto = $idpresupuesto";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idpresupuesto=-1, $idgramaje=-1, $idtamano=-1, 
            $descripcion='', $color=0, $doblefaz=0,
            $cantidad=0, $precio=0){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitimpresionlaser(0,-1,
                $idpresupuesto, $idgramaje, $idtamano,
                '$descripcion', $color, $doblefaz,
                $cantidad, $precio, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idpresupuesto=-1, $idgramaje=-1, $idtamano=-1,
            $descripcion='', $color=0, $doblefaz=0,
            $cantidad=0, $precio=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitimpresionlaser(1, $id,
                $idpresupuesto, $idgramaje, $idtamano,
                '$descripcion', $color, $doblefaz,
                $cantidad, $precio, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitimpresionlaser(2, $id,
                0, 0, 0, 
                '', 0, 0,
                0, 0, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return 0;
    }
}