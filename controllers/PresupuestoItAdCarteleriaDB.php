<?php
/**
 * Description of PresupuestoItAdCarteleriaDB
 *
 * @author meza
 */
class PresupuestoItAdCarteleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestositsadscarteleria';
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idpresitcarteleria, i.idtipoadicional, t.tipoadicional,
                    i.ancho, i.alto, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadscarteleria i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional 
                WHERE i.id=$id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT i.id, i.idpresitcarteleria, i.idtipoadicional, t.tipoadicional,
                        i.ancho, i.alto, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadscarteleria i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPresItCarteleria($idpresitcarteleria) {
        $query = "SELECT i.id, i.idpresitcarteleria, i.idtipoadicional, t.tipoadicional,
                        i.ancho, i.alto, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadscarteleria i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional 
                WHERE i.idpresitcarteleria = $idpresitcarteleria";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idpresitcarteleria=-1, $idtipoadicional=-1, $ancho=0, 
            $alto=0, $unidades=0, $precio=0){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadcarteleria(0,-1,
                $idpresitcarteleria, $idtipoadicional, $ancho, 
                $alto, $unidades, $precio,
                @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idpresitcarteleria=-1, $idtipoadicional=-1, $ancho=0, 
            $alto=0, $unidades=0, $precio=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadcarteleria(1, $id,
                $idpresitcarteleria, $idtipoadicional, $ancho, 
                $alto, $unidades, $precio,
                @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadcarteleria(2, $id,
                0, 0, 0, 
                0, 0, 0, 
                @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return 0;
    }
}