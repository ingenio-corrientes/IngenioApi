<?php
/**
 * Description of ClienteDB
 *
 * @author meza
 */
class ClienteDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'clientes';
   
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query= "SELECT l.id, l.razonsocial, l.telefono, l.email, l.direccion, l.idtipodecliente, l.clientetipo, 
                l.cuit, l.fecultmodif, b.borrador, p.produccion, e.emitido 
            FROM 
                (SELECT c.id, c.razonsocial, c.telefono, c.email, c.direccion, c.idtipodecliente, 
                    t.clientetipo, c.cuit, c.fecultmodif 
                FROM clientes c 
                LEFT JOIN clientestipo t ON c.idtipodecliente = t.id) l 
            LEFT JOIN 
                (SELECT c.id, IFNULL((CASE WHEN MIN(p.fecemision) = CONVERT(0, DATETIME) THEN COUNT(p.id) END), 0) AS borrador 
                FROM clientes c 
                LEFT JOIN presupuestos p ON p.idcliente = c.id 
                GROUP BY c.id) b ON b.id = l.id 
            LEFT JOIN 
                (SELECT c.id, IFNULL((CASE WHEN MIN(p.fecaproduccion) <> CONVERT(0, DATETIME) AND MIN(p.fecentrega) = CONVERT(0, DATETIME) THEN COUNT(p.id) END), 0) AS produccion 
                FROM clientes c 
                LEFT JOIN presupuestos p ON p.idcliente = c.id 
                GROUP BY c.id) p ON p.id = l.id 
            LEFT JOIN 
                (SELECT c.id, IFNULL((CASE WHEN MIN(p.fecemision) <> CONVERT(0, DATETIME) AND MIN(p.fecaproduccion) = CONVERT(0, DATETIME) THEN COUNT(p.id) END), 0) AS emitido 
                FROM clientes c 
                LEFT JOIN presupuestos p ON p.idcliente = c.id 
            GROUP BY c.id) e ON e.id = l.id;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getCuentaCorriente($idcliente=-1){
        $query = "SELECT cc.* 
            FROM 
                (SELECT p.idcliente, c.razonsocial, p.id AS idpresupuesto, 
                IFNULL(p.fecentrega, '') AS fecentrega, p.saldo, p.total AS monto
                    FROM presupuestos p 
                    LEFT JOIN clientes c ON c.id = p.idcliente
                    LEFT JOIN recibos r ON r.idpresupuesto = p.id
                    LEFT JOIN recibositems i ON i.idrecibo = r.id
                    WHERE p.idcliente = $idcliente 
                        AND p.fecemision > CONVERT(0,DATETIME)
                        AND p.actacte = 1
                    GROUP BY p.id) cc
                WHERE cc.saldo > 0;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function insert(
            $razonsocial='', $telefono='', $email='', 
            $direccion='', $idtipodecliente=-1, $cuit=-1){
        $query= "INSERT INTO " . self::TABLE . " ("
                . "razonsocial, telefono, email, "
                . "direccion, idtipodecliente, cuit, "
                . "fecultmodif) "
                . "VALUES ("
                . "'$razonsocial', '$telefono', '$email', "
                . "'$direccion', $idtipodecliente, $cuit, NOW());";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }

        public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function update($id=-1, $razonsocial='', $telefono='', 
        $email='', $direccion='', $idtipodecliente=-1, $cuit=-1) {
        
        if($this->checkIntID(self::TABLE, $id)){
            $query = "UPDATE " . self::TABLE . " SET "
                    . "razonsocial= '$razonsocial', telefono= '$telefono', "
                    . "email= '$email', direccion= '$direccion', idtipodecliente= $idtipodecliente, "
                    . "cuit= $cuit, fecultmodif= NOW() "
                    . "WHERE id = $id;";
            //var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }

}
