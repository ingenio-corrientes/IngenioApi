<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrecioAPI
 *
 * @author meza
 */
class PrecioAPI extends EntityAPI {
    const API_ACTION = 'precio';
	
     public function API(){
        header('Content-Type: application/JSON');    
        $method = $_SERVER['REQUEST_METHOD'];
		$this->db = new PrecioDB();
		
        switch ($method) {
            case 'GET'://consulta
                $this->processGet();
                break;
            case 'POST'://inserta
                $this->processPost();
                break;
            case 'PUT'://actualiza
                $this->processPut();
                break;
            case 'DELETE'://elimina
                $this->processDelete();
                break;
            default://metodo NO soportado
                echo 'METODO NO SOPORTADO';
                break;
        }
    }
    
    function processGet(){
        if($_GET['action']==self::API_ACTION){
            //$db = new ObraDB();
            if(isset($_GET['id'])){
                $response = $this->db->getById($_GET['id']);
                echo json_encode($response,JSON_PRETTY_PRINT);
            }else{
                $response = $this->db->getList();
                echo json_encode($response,JSON_PRETTY_PRINT);
            }
        }else{
                $this->response(400);
        }
    }
    
    function processPost() {
        if($_GET['action']==self::API_ACTION)
        {
            $obj = json_decode( file_get_contents('php://input') );
            $objArr = (array)$obj;
            if (empty($objArr)) {
                $this->response(422,"error","Nothing to add. Check json");
            } else if(isset($obj->precio) AND isset($obj->idtiposerv) 
                    AND isset($obj->tipoclient) AND isset($obj->desyreca)
                    AND isset($obj->monto) AND isset($obj->idlistabase) AND isset($obj->fecultmodif)) {
                $r = $this->db->insert(
						$obj->precio, $obj->idtiposerv, 
                        $obj->tipoclient, $obj->desyreca, 
						$obj->monto, $obj->idlistabase, $obj->fecultmodif);
                if($r)
                    $this->response(200,"success","new record added");
            } else {
                $this->response(422,"error","The property is not defined");
            } 
        }
        else
        {
            $this->response(400);
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->precio) AND isset($obj->idtiposerv) 
                    AND isset($obj->tipoclient) AND isset($obj->desyreca)
                    AND isset($obj->monto) AND isset($obj->idlistabase) AND isset($obj->fecultmodif)){
                    if($this->db->update($_GET['id'], 
						$obj->precio, $obj->idtiposerv, 
                        $obj->tipoclient, $obj->desyreca, 
						$obj->monto, $obj->idlistabase, $obj->fecultmodif))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }
    function processDelete() {
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
    }