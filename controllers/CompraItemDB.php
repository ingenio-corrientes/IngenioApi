<?php
/**
 * Description of CompraItemDB
 *
 * @author meza
 */
class CompraItemDB  extends EntityDB {
   protected $mysqli;
   const TABLE = 'comprasitems';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id='$id';");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM ". self::TABLE);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByItemsIdCompra($idcompra) {
        $query = "SELECT i.id, i.idcompra, i.descripcion, i.precio, i.cantidad
            FROM comprasitems i
            WHERE idcompra = '$idcompra';";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert(
            $idcompra='', $descripcion='', $precio=0, 
            $cantidad=0){
        $id = $this->gen_uuid();
        $query = "INSERT INTO comprasitems
                    (id, 
                    idcompra, descripcion, precio, 
                    cantidad, fecultmodif) 
                VALUES 
                    ('$id', 
                    '$idcompra', '$descripcion', $precio, 
                    $cantidad, NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    
    public function update($id='', 
            $idcompra='', $descripcion='', $precio=0, 
            $cantidad=0) {
        $query = "UPDATE comprasitems SET 
                idcompra = '$idcompra', descripcion = '$descripcion', precio = $precio,
                cantidad = $cantidad, fecultmodif = NOW() 
            WHERE id = '$id';";
//        var_dump($query);
        if($this->checkStringID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id='') {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}