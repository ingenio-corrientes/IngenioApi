<?php
/**
 * Description of LPrecioPapeleriaIntervaloDB
 *
 * @author meza
 */
class LPrecioPapeleriaIntervaloDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'lpreciospapeleriasintervalos';
    
    public function getList(){
        $query = "SELECT i.id, i.idlpreciopapeleria, l.precio AS preciobase, i.maximo, i.porcentaje, 
                l.precio * ( 1 + i.porcentaje / 100 ) AS precio, t.tipotrabajopapeleria, 
                (CASE t.comercial  WHEN 1 
                THEN Concat(g.gramaje, '|', c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial, '|', m.tamano) 
                ELSE Concat(g.gramaje, '|', p.tipocorte) END) AS descripcion 
            FROM lpreciospapeleriasintervalos i 
            LEFT JOIN lpreciospapelerias l ON l.id = i.idlpreciopapeleria 
            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo 
            LEFT JOIN gramajes g ON g.id = l.idgramaje 
            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom 
            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial 
            LEFT JOIN tamanos m ON m.id = l.idtamano 
            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte 
            ORDER BY i.idlpreciopapeleria, i.maximo;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idlpreciopapeleria, l.precio AS preciobase, i.maximo, i.porcentaje, 
                l.precio * ( 1 + i.porcentaje / 100 ) AS precio, t.tipotrabajopapeleria, 
                (CASE t.comercial  WHEN 1 
                THEN Concat(g.gramaje, '|', c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial, '|', m.tamano) 
                ELSE Concat(g.gramaje, '|', p.tipocorte) END) AS descripcion 
            FROM lpreciospapeleriasintervalos i 
            LEFT JOIN lpreciospapelerias l ON l.id = i.idlpreciopapeleria 
            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo 
            LEFT JOIN gramajes g ON g.id = l.idgramaje 
            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom 
            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial 
            LEFT JOIN tamanos m ON m.id = l.idtamano 
            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte
            WHERE l.id=$id 
            ORDER BY i.idlpreciopapeleria, i.maximo;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdLista($idlista) {
        $query = "SELECT i.id, i.idlpreciopapeleria, l.precio AS preciobase, i.maximo, i.porcentaje, 
                l.precio * ( 1 + i.porcentaje / 100 ) AS precio, t.tipotrabajopapeleria, 
                (CASE t.comercial  WHEN 1 
                THEN Concat(c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial) 
                ELSE Concat(g.gramaje) END) AS descripcion 
            FROM lpreciospapeleriasintervalos i 
            LEFT JOIN lpreciospapelerias l ON l.id = i.idlpreciopapeleria 
            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo 
            LEFT JOIN gramajes g ON g.id = l.idgramaje 
            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom 
            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial 
            LEFT JOIN tamanos m ON m.id = l.idtamano 
            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte
            WHERE i.idlpreciopapeleria = $idlista 
            ORDER BY i.idlpreciopapeleria, i.maximo;";
//        $query = "SELECT i.id, i.idlpreciopapeleria, l.precio AS preciobase, i.maximo, i.porcentaje, 
//                l.precio * ( 1 + i.porcentaje / 100 ) AS precio, t.tipotrabajopapeleria, 
//                (CASE t.comercial  WHEN 1 
//                THEN Concat(g.gramaje, '|', c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial, '|', m.tamano) 
//                ELSE Concat(g.gramaje, '|', p.tipocorte) END) AS descripcion 
//            FROM lpreciospapeleriasintervalos i 
//            LEFT JOIN lpreciospapelerias l ON l.id = i.idlpreciopapeleria 
//            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo 
//            LEFT JOIN gramajes g ON g.id = l.idgramaje 
//            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom 
//            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial 
//            LEFT JOIN tamanos m ON m.id = l.idtamano 
//            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte
//            WHERE i.idlpreciopapeleria = $idlista 
//            ORDER BY i.idlpreciopapeleria, i.maximo;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idlpreciopapeleria, $maximo, $porcentaje){
        $query = "INSERT INTO " . self::TABLE . " "
                . "(idlpreciopapeleria, maximo, porcentaje, fecultmodif) "
                . "VALUES "
                . "($idlpreciopapeleria, $maximo, $porcentaje, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idlpreciopapeleria, $maximo, $porcentaje) {
        $query = "UPDATE " . self::TABLE . " SET "
                . "idlpreciopapeleria=$idlpreciopapeleria, maximo=$maximo, "
                . "porcentaje=$porcentaje, fecultmodif=NOW() "
                . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}