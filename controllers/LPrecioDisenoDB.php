<?php
/**
 * Description of LPrecioCarteleriaDB
 *
 * @author meza
 */
class LPrecioDisenoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'lpreciosdisenos';
    
    public function getList(){
        $query = "SELECT l.id, l.idlistaprecio, 
                d.id AS idtipodiseno, d.tipodiseno, 
                l.idsubtipodiseno, t.subtipodiseno, l.precio, l.fecultmodif
            FROM lpreciosdisenos l
            LEFt JOIN subtiposdisenos t ON t.id = l.idsubtipodiseno
            LEFT JOIN tiposdisenos d ON d.id = t.idtipodiseno";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getById($id=0){
        $query = "SELECT l.id, l.idlistaprecio, 
                d.id AS idtipodiseno, d.tipodiseno, 
                l.idsubtipodiseno, t.subtipodiseno, l.precio, l.fecultmodif
            FROM lpreciosdisenos l
            LEFt JOIN subtiposdisenos t ON t.id = l.idsubtipodiseno
            LEFT JOIN tiposdisenos d ON d.id = t.idtipodiseno
            WHERE id=$id;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdLista($idlista) {
        $query = "SELECT l.id, l.idlistaprecio, 
                d.id AS idtipodiseno, d.tipodiseno, 
                l.idsubtipodiseno, t.subtipodiseno, l.precio, l.fecultmodif
            FROM lpreciosdisenos l
            LEFt JOIN subtiposdisenos t ON t.id = l.idsubtipodiseno
            LEFT JOIN tiposdisenos d ON d.id = t.idtipodiseno
            WHERE l.idlistaprecio = $idlista";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idlistaprecio, $idsubtipodiseno, $precio){
        $query = "INSERT INTO " . self::TABLE . " "
                . "(idlistaprecio, idsubtipodiseno, precio, fecultmodif) "
                . "VALUES "
                . "($idlistaprecio, $idsubtipodiseno, $precio, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idlistaprecio, $idsubtipodiseno, $precio) {
        $query = "UPDATE " . self::TABLE . " SET "
                . "idlistaprecio=$idlistaprecio, idsubtipodiseno=$idsubtipodiseno, precio=$precio, "
                . "fecultmodif=NOW() "
                . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function updateLista($id=-1) {
        $sp_insertnew = "CALL update_lpreciosdiseno($id);";
//        var_dump($sp_insertnew);
//        return true;
        $r = $this->mysqli->query($sp_insertnew);
        return $this->mysqli->errno;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}