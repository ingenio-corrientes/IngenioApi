<?php
/**
 * Description of ReporteDB
 *
 * @author meza
 */
class ReporteDB extends EntityDB {
   protected $mysqli;
   const TABLE = '';
	
    public function getResumen(){
        $query = "SELECT a.enproduccion, b.noaprobados, c.caja, d.clientectacte
            FROM 
                (SELECT COUNT(id) AS enproduccion
                FROM presupuestos
                WHERE fecaproduccion > '0000-00-00') a
            JOIN 
                (SELECT COUNT(id) AS noaprobados
                FROM presupuestos
                WHERE fecemision > '0000-00-00' AND fecaproduccion = '0000-00-00') b
            JOIN
                (SELECT SUM(i.monto) AS caja
                FROM recibositems i 
                LEFT JOIN recibos r ON r.id = i.idrecibo
                WHERE r.fecha >= NOW()) c
            JOIN
                (SELECT COUNT(p.idcliente) AS clientectacte
                FROM presupuestos p
                WHERE p.saldo > 0 AND p.fecentrega > '0000-00-00'
                GROUP BY p.idcliente) d";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
}