<?php
/**
 * Description of ModuloAPI
 *
 * @author meza
 */
class ModuloAPI extends EntityAPI {
    const API_ACTION = 'modulo';

    public function __construct() {
	$this->db = new ModuloDB();
    }
	
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    } 
	
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(isset($obj->modulo)) {
            $r = $this->db->insert($obj->modulo);
            if($r) { $this->response(200,"success","new record added"); }
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');        
        if(!$id){
            $this->response(400, "error", "The id not defined");
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)){
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(isset($obj->modulo)) {
            if($this->db->update($id, $obj->modulo)) {
                $this->response(200,"success","Record updated");
            } else {
                $this->response(304,"success","Record not updated");
            }
        }else{
            $this->response(422,"error","The property is not defined");
        }
    }
}