<?php
/**
 * Description of RegistroCajaAPI
 *
 * @author meza
 */
class RegistroCajaAPI extends EntityAPI {
    const GET_REGISTRO = 'registro';
    const GET_CHEQUESENCARTERA = 'chequesencartera';
    const GET_TOTALES = 'totales';
    const GET_FORMASCOBROPAGO = 'formascobropago';
    const GET_RETIROSEFECTIVO= 'retirosefectivos';
    const GET_INFORMEGENERAL= 'informegeneral';
    const GET_INFORMECOBROPAGO= 'informecobropago';
    const API_ACTION = 'registrocaja';

    public function __construct() {
        $this->db = new RegistroCajaDB();
        $this->fields = [];
        array_push($this->fields, 
                'fecinicio',
                'efectivoinicio',
                'fecarqueo',
                'efectivoarqueo');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isRegistro = isset($id) ? ($id === self::GET_REGISTRO) : false;
        $isChequesEnCartera = isset($id) ? ($id === self::GET_CHEQUESENCARTERA) : false;
        $isTotales = isset($id) ? ($id === self::GET_TOTALES) : false;
        $isFormasCobroPago = isset($id) ? ($id === self::GET_FORMASCOBROPAGO) : false;
        $isRetirosEfectivo = isset($id) ? ($id === self::GET_RETIROSEFECTIVO) : false;
        $isInformeGeneral = isset($id) ? ($id === self::GET_INFORMEGENERAL) : false;
        $isInformeCobroPago = isset($id) ? ($id === self::GET_INFORMECOBROPAGO) : false;
        
        if($isRegistro) {
            $response = $this->db->getRegistro();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isChequesEnCartera) {
            $response = $this->db->getChequesEnCartera();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isTotales) {
            $response = $this->db->getTotales();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isFormasCobroPago) {
            $response = $this->db->getFormasCobroPago();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isRetirosEfectivo) {
            $response = $this->db->getRetirosEfectivo();
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isInformeGeneral) {
            $fecDesde = filter_input(INPUT_GET, 'fld1');
            $fecHasta = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getInformeGeneral($fecDesde, $fecHasta);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif ($isInformeCobroPago) {
            $fecDesde = filter_input(INPUT_GET, 'fld1');
            $fecHasta = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getInformeCobroPago($fecDesde, $fecHasta);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->fecinicio, $obj->efectivoinicio, 
                $obj->fecarqueo, $obj->efectivoarqueo);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->fecinicio, $obj->efectivoinicio, 
                $obj->fecarqueo, $obj->efectivoarqueo);
        if($r) { $this->response(200,"success",$r); }
        else { $this->response(204,"success","Record not updated");}
    }
}
