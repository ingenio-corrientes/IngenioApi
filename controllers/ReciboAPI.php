<?php
/**
 * Description of ReciboAPI
 *
 * @author meza
 */
class ReciboAPI extends EntityAPI {
    const API_ACTION = 'recibo';
    const GET_FILTEREDBYDATES = 'filteredbydates';
    const PUT_EMITIR = 'emitir';

    public function __construct() {
        $this->db = new ReciboDB();
        $this->fields = [];
        array_push($this->fields, 
                'idcliente', 
                'fecha', 
                'idpresupuesto', 
                'fecemision');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isFilteredByDates = isset($id) ? $id === self::GET_FILTEREDBYDATES : false;
        
        if($isFilteredByDates) {
            $fecinicio = filter_input(INPUT_GET, 'fld1');
            $fecfin = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getFilteredByDates($fecinicio, $fecfin);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idcliente, $obj->fecha, $obj->idpresupuesto, 
            $obj->fecemision);
        if($r) {$this->response(200,"success",$r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        $isEmitir =  isset($id) ? ($id === self::PUT_EMITIR) : false;

        if ($isEmitir) {
            $this->processEmitir();
        } else {
            $this->processStandartPut();
        }
    }
    
    function processStandartPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
            $obj->idcliente, $obj->fecha, $obj->fecemision, 
            $obj->idpresupuesto);
        
        if($r) { $this->response(200,"success",$id); }
        else { $this->response(204,"success",$id);}
    }
    
    function processEmitir() {
        $id = filter_input(INPUT_GET, 'fld1');
        $r = $this->db->emitir($id);
        if($r) { $this->response(200,"success", $r); }
        else { $this->response(204,"success", $r);}
    }
}
