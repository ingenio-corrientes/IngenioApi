<?php
/**
 * Description of ProveedorAPI
 *
 * @author meza
 */
class ProveedorAPI extends EntityAPI {
    const API_ACTION = 'proveedor';	
    const GET_CUENTACORRIENTEPROVEEDOR = 'cuentacorrienteproveedor';

    public function __construct() {
	$this->db = new ProveedorDB();
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $iscuentacorrienteproveedor = isset($id) ? $id === self::GET_CUENTACORRIENTEPROVEEDOR : false;
        
        if($iscuentacorrienteproveedor) {
            $idproveedor = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getCuentaCorrienteProveedor($idproveedor);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
        } else if(isset($obj->razonsocial) AND isset($obj->cuit) 
                AND isset($obj->telefono) AND isset($obj->email) 
                AND isset($obj->direccion)) {
            $r = $this->db->insert($obj->razonsocial, $obj->cuit, 
                    $obj->telefono, $obj->email, $obj->direccion);
            if($r)
                $this->response(200,"success","new record added");
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id){
            $this->response(400,"error","The ID is not defined");
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if (empty($objArr)){                        
            $this->response(422,"error","Nothing to add. Check json");                        
        }else if(isset($obj->razonsocial) AND isset($obj->cuit) 
            AND isset($obj->telefono) AND isset($obj->email) 
            AND isset($obj->direccion)){
            if($this->db->update($_GET['id'], $obj->razonsocial, $obj->cuit, 
                $obj->telefono, $obj->email, $obj->direccion))
                $this->response(200,"success","Record updated");                             
            else
                $this->response(304,"success","Record not updated");                             
        }else{
            $this->response(422,"error","The property is not defined");                        
        }
    }
    
    function processDelete() {
        
        if (isset($_GET['action']) && isset($_GET['id'])) {
            if ($_GET['action'] == self::API_ACTION) {
                $this->db->delete($_GET['id']);
                $this->response(204);
                exit;
            }
        }
        $this->response(400);
    }
}
