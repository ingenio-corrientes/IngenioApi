<?php
/**
 * Description of LPrecioCarteleriaDB
 *
 * @author meza
 */
class LPrecioCarteleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'lprecioscartelerias';
    
    public function getList(){
        $query = "SELECT l.id, l.idlistaprecio, 
                s.id AS idsustrato, s.sustrato, 
                l.idsubtiposustrato, t.subtiposustrato, l.precio, l.fecultmodif
            FROM lprecioscartelerias l
            LEFt JOIN subtipossustratos t ON t.id = l.idsubtiposustrato
            LEFT JOIN sustratos s ON s.id = t.idsustrato";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getById($id=0){
        $query = "SELECT l.id, l.idlistaprecio, 
                s.id AS idsustrato, s.sustrato, 
                l.idsubtiposustrato, t.subtiposustrato, l.precio, l.fecultmodif
            FROM lprecioscartelerias l
            LEFt JOIN subtipossustratos t ON t.id = l.idsubtiposustrato
            LEFT JOIN sustratos s ON s.id = t.idsustrato
            WHERE l.id=$id;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdLista($idlista) {
        $query = "SELECT l.id, l.idlistaprecio, 
                s.id AS idsustrato, s.sustrato, 
                l.idsubtiposustrato, t.subtiposustrato, l.precio, l.fecultmodif
            FROM lprecioscartelerias l
            LEFt JOIN subtipossustratos t ON t.id = l.idsubtiposustrato
            LEFT JOIN sustratos s ON s.id = t.idsustrato
            WHERE l.idlistaprecio = $idlista";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idlistaprecio, $idsubtiposustrato, $precio){
        $query = "INSERT INTO " . self::TABLE . " "
                . "(idlistaprecio, idsubtiposustrato, precio, fecultmodif) "
                . "VALUES "
                . "($idlistaprecio, $idsubtiposustrato, $precio, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idlistaprecio=-1, $idsubtiposustrato=-1, $precio=-1) {
        $query = "UPDATE " . self::TABLE . " SET "
                . "idlistaprecio=$idlistaprecio, idsubtiposustrato=$idsubtiposustrato, precio=$precio, "
                . "fecultmodif=NOW() "
                . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function updateLista($id=-1) {
        $sp_insertnew = "CALL update_lprecioscartelerias($id);";
        $r = $this->mysqli->query($sp_insertnew);
        return $this->mysqli->errno;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}