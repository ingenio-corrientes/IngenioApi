<?php
/**
 * Description of GastoDB
 *
 * @author meza
 */
class GastoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'gastos';
    
    public function getById($id=0){
        $query = "SELECT g.id, g.idproveedor, p.razonsocial, g.fecha, 
                    g.concepto, g.fecemision, g.fecultmodif, IFNULL(i.monto, 0) AS monto,
                    IFNULL(d.totaldeuda, 0) AS totaldeuda, g.dirty, 
                    (CASE g.dirty WHEN 1 THEN 'Borrador' ELSE '' END) AS borrador 
                FROM gastos g 
                LEFT JOIN proveedores p ON g.idproveedor = p.id 
                LEFT JOIN (SELECT idgasto, SUM(monto) AS monto 
                    FROM gastositems 
                    GROUP BY idgasto) i ON i.idgasto = g.id 
                LEFT JOIN (SELECT idgasto, SUM(montoacancelar) AS totaldeuda
                    FROM gastosdeudas
                    GROUP BY idgasto) d ON d.idgasto = g.id 
                WHERE g.id = $id";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getFilteredByDates($fecinicio=0, $fecfin=0){
        $query = "SELECT g.id, g.idproveedor, p.razonsocial, g.fecha, 
                g.concepto, g.fecemision, g.fecultmodif, IFNULL(i.monto, 0) AS monto, g.dirty, 
                (CASE g.dirty WHEN 1 THEN 'Borrador' ELSE '' END) AS borrador 
            FROM gastos g 
            LEFT JOIN proveedores p ON g.idproveedor = p.id 
            LEFT JOIN 
                (SELECT idgasto, SUM(monto) as monto 
                FROM gastositems 
                GROUP BY idgasto) i ON i.idgasto = g.id 
            WHERE g.fecha BETWEEN date('$fecinicio')
                AND date('$fecfin')";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT g.id, g.idproveedor, p.razonsocial, g.fecha, g.concepto, 
                g.fecemision, g.fecultmodif, IFNULL(i.monto, 0) AS monto, g.dirty, 
                (CASE g.dirty WHEN 1 THEN 'Borrador' ELSE '' END) AS borrador 
            FROM gastos g 
            LEFT JOIN proveedores p ON g.idproveedor = p.id 
            LEFT JOIN 
                (SELECT idgasto, SUM(monto) as monto 
                FROM gastositems 
                GROUP BY idgasto) i ON i.idgasto = g.id";
//            var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert( $idproveedor=-1, $fecha='',  $concepto=''){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_gasto(-1,0, $idproveedor, 
                '$fecha', '$concepto', @p_oresult);";
//        var_dump($query);
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idproveedor=-1, $fecha='', $concepto='') {
        
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        $query = "CALL abm_gasto($id,1, $idproveedor, 
                '$fecha', '$concepto', @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
    
    public function emitir($id=-1) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        $query = "CALL sp_gastoemitir($id, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_newid:= -1");
        $stmt->execute();
        $query = "CALL abm_gasto($id, 2, 0,
                '', '', @p_newid);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === '-1' ) {
            return false;
        }
        return 0;
    }
    
    public function pagar($idproveedor, $idcompraapagar) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        $query = "CALL sp_gastopagarcompra($idproveedor, '$idcompraapagar', @p_oresult);";
//        var_dump($query);
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
//        var_dump($row);
        return $row['p_oresult'];
    }
}