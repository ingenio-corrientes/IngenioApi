<?php
/**
 * Description of TipoPapeleriaCorteDB
 *
 * @author meza
 */
class TipoPapeleriaCorteDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tipospapeleriascortes';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM ". self::TABLE);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($tiposcortes=""){
        $stmt = $this->mysqli->prepare(
            "INSERT INTO tipospapeleriascortes (tiposcortes) 
            VALUES ('$tiposcortes');");
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $tiposcortes="") {
        $query = "UPDATE tipospapeleriascortes 
            SET tiposcortes='$tiposcortes' 
            WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }	
}
