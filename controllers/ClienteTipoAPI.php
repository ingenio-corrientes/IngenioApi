<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClienteTipoAPI
 *
 * @author meza
 */
class ClienteTipoAPI  extends EntityAPI {
    const API_ACTION = 'clientetipo';    

    public function __construct() {
	$this->db = new ClienteTipoDB();
    }
	
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if(isset($id)){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    } 
	
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
        } else if(isset($obj->clientetipo)) {
            $r = $this->db->insert($obj->clientetipo);
            if($r) { $this->response(200,"success","new record added"); }
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');        
        if(!isset($id)){
            $this->response(400);
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if (empty($objArr)){
            $this->response(422,"error","Nothing to add. Check json");  
            exit;
        }
        
        if(isset($obj->clientetipo)) {
            if($this->db->update($id, $obj->clientetipo)) {
                $this->response(200,"success","Record updated");
            } else {
                $this->response(304,"success","Record not updated");
            }
        }else{
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processDelete() {
        $id = filter_input(INPUT_GET, 'id');
        if(!isset($id)){
            $this->response(400);
            exit;
        }
        $this->db->delete($_GET['id']);
        $this->response(204,"success","Record was deleted");
    }
}