<?php
/**
 * Description of ReciboDB
 *
 * @author meza
 */
class ReciboDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'recibos';
    
    public function getById($id=0){
        $query = "SELECT r.id, r.idcliente, c.razonsocial, r.fecha, IFNULL(r.fecemision, '') AS fecemision, 
                r.idpresupuesto, r.fecultmodif, IFNULL(i.monto, 0) AS monto, 
                p.total AS montopresupuesto, p.saldo AS saldopresupuesto,
                (CASE r.fecemision IS NULL WHEN TRUE THEN 'SI' ELSE '' END) AS borrador 
            FROM recibos r 
            LEFT JOIN clientes c ON r.idcliente = c.id 
            LEFT JOIN presupuestos p ON p.id = r.idpresupuesto
            LEFT JOIN 
                (SELECT idrecibo, SUM(monto) as monto 
                FROM recibositems 
                GROUP BY idrecibo) i ON i.idrecibo = r.id 
            WHERE r.id = $id";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getFilteredByDates($fecinicio=0, $fecfin=0){
        $query = "SELECT r.id, r.idcliente, c.razonsocial, r.fecha, r.fecemision, 
                r.idpresupuesto, r.fecultmodif, IFNULL(i.monto, 0) AS monto, 
                IFNULL(p.saldo, 0) AS saldopresupuesto,
                (CASE r.fecemision IS NULL WHEN TRUE THEN 'SI' ELSE '' END) AS borrador 
            FROM recibos r 
            LEFT JOIN clientes c ON r.idcliente = c.id 
            LEFT JOIN 
                (SELECT idrecibo, SUM(monto) as monto 
                FROM recibositems 
                GROUP BY idrecibo) i ON i.idrecibo = r.id
            WHERE r.fecha BETWEEN date('$fecinicio')
                AND date('$fecfin')";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT r.id, r.idcliente, c.razonsocial, r.fecha, r.fecemision, 
                r.idpresupuesto, r.fecultmodif, IFNULL(i.monto, 0) AS monto, 
                (CASE r.fecemision IS NULL WHEN TRUE THEN 'SI' ELSE '' END) AS borrador, 
                p.saldo AS saldopresupuesto
            FROM recibos r 
            LEFT JOIN clientes c ON r.idcliente = c.id 
            LEFT JOIN presupuestos p ON p.id = r.idpresupuesto
            LEFT JOIN 
                (SELECT idrecibo, SUM(monto) as monto 
                FROM recibositems 
                GROUP BY idrecibo) i ON i.idrecibo = r.id;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idcliente=-1, $fecha='', $idpresupuesto=-1, 
            $fecemision=''){
        $query = "INSERT INTO " . self::TABLE . " 
                (idcliente, fecha, idpresupuesto, fecemision, fecultmodif) 
                VALUES ($idcliente, '$fecha', $idpresupuesto, null, NOW());";
//        var_dump($query);
//        return 1;
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idcliente=-1, $fecha=null, $fecemision=null,
            $idpresupuesto=-1) {
        $fecemision=isset($fecemision) ? 'NULL' : "'$fecemision'";
        
        $query = "UPDATE recibos 
            SET idcliente=$idcliente, fecha='$fecha', fecemision= $fecemision, 
                idpresupuesto=$idpresupuesto, fecultmodif = NOW() 
            WHERE id = $id;";
//        var_dump($query);
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return true;
    }
    
    public function emitir($id=-1) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL sp_emitir_recibo($id, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];

//        $query = "UPDATE recibos SET
//                fecemision= NOW(), fecultmodif= NOW() 
//            WHERE id = $id;"; 
////        var_dump($query);
//        if($this->checkIntID(self::TABLE, $id)){
//            $stmt = $this->mysqli->prepare($query);
//            $r = $stmt->execute(); 
//            $stmt->close();
//            return $r;
//        }
//        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}

