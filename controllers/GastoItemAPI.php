<?php
/**
 * Description of GastoItemAPI
 *
 * @author meza
 */
class GastoItemAPI extends EntityAPI {
    const GET_LISTBYIDGASTO = 'byidgasto';
    const API_ACTION = 'gastoitem';

    public function __construct() {
	$this->db = new GastoItemDB();
        $this->fields = [];
        array_push($this->fields, 
                'idgasto', 
                'idformapago', 
                'idbanco', 
                'origendestino',
                'idcheque',
                'numero', 
                'fecha', 
                'fecvencimiento',
                'propio',
                'monto');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdGasto = strpos($id, self::GET_LISTBYIDGASTO);
        
        if($isByIdGasto !== false) {
            $idgasto = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdGasto($idgasto);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id !== false){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
   
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idgasto, $obj->idformapago, $obj->idbanco, 
                $obj->origendestino, $obj->idcheque, $obj->numero,
                $obj->fecha, $obj->propio, $obj->fecvencimiento, 
                $obj->monto);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idgasto, $obj->idformapago, $obj->idbanco, 
                $obj->origendestino, $obj->idcheque, $obj->numero,
                $obj->fecha, $obj->propio, $obj->fecvencimiento,
                $obj->monto);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}
