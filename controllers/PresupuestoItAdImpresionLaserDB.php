<?php
/**
 * Description of PresupuestoItAdImpresionLaserDB
 *
 * @author meza
 */
class PresupuestoItAdImpresionLaserDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestositsadsimpresionlaser';
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idpresitimpresionlaser, i.idtipoadicional, 
                t.tipoadicional, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadsimpresionlaser i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional 
                WHERE i.id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT i.id, i.idpresitimpresionlaser, i.idtipoadicional, 
                    t.tipoadicional, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadsimpresionlaser i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPresItImpresionLaser($idpresitimpresionlaser) {
        $query = "SELECT i.id, i.idpresitimpresionlaser, i.idtipoadicional, 
                    t.tipoadicional, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadsimpresionlaser i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional 
                WHERE i.idpresitimpresionlaser = $idpresitimpresionlaser";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idpresitimpresionlaser=-1, $idtipoadicional=-1, $unidades=-1, 
            $precio=0){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadimpresionlaser(0,-1,
                $idpresitimpresionlaser, $idtipoadicional, $unidades, 
                $precio, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idpresitimpresionlaser=-1, $idtipoadicional=-1, $unidades=0, 
            $precio=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadimpresionlaser(1, $id,
                $idpresitimpresionlaser, $idtipoadicional, $unidades, 
                $precio, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadimpresionlaser(2, $id,
                0, 0, 0, 
                0, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return 0;
    }
}