<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BancoDB
 *
 * @author meza
 */
class BancoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'bancos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM ". self::TABLE);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($banco=""){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (banco) "
                . "VALUES (?);");
        $stmt->bind_param('s', $banco);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }

    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function update($id=-1, $banco="") {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET banco=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('si', $banco,$id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}