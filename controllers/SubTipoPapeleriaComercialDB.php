<?php
/**
 * Description of SubTipoPapeleriaComercialDB
 *
 * @author meza
 */
class SubTipoPapeleriaComercialDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'subtipospapeleriascomerciales';
    
    public function getById($id=0){
        $query = "SELECT s.id, s.idtipopapeleriacomercial, s.subtipopapeleriacomercial, t.tipopapeleriacomercial
            FROM subtipospapeleriascomerciales s
            LEFT JOIN tipospapeleriascomerciales t On t.id = s.idtipopapeleriacomercial
            WHERE s.id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query( 
            "SELECT s.id, s.idtipopapeleriacomercial, s.subtipopapeleriacomercial, t.tipopapeleriacomercial
            FROM subtipospapeleriascomerciales s
            LEFT JOIN tipospapeleriascomerciales t On t.id = s.idtipopapeleriacomercial;");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($idtipopapeleriacomercial,
            $subtipopapeleriacomercial){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE 
                . " (idtipopapeleriacomercial, subtipopapeleriacomercial) "
                . "VALUES"
                . " ($idtipopapeleriacomercial, '$subtipopapeleriacomercial');");
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $idtipopapeleriacomercial,
            $subtipopapeleriacomercial) {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET "
                    . "idtipopapeleriacomercial = $idtipopapeleriacomercial, "
                    . "subtipopapeleriacomercial = '$subtipopapeleriacomercial' "
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
