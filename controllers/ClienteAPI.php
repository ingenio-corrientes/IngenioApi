<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClienteAPI
 *
 * @author meza
 */
class ClienteAPI extends EntityAPI {
    const API_ACTION = 'cliente';
    const GET_CUENTACORRIENTE = 'cuentacorriente';

    
    public function __construct() {
        $this->db = new ClienteDB();
        $this->fields = [];
        array_push($this->fields, 
                'razonsocial',
                'telefono',
                'email',
                'direccion',
                'idtipodecliente',
                'cuit');
    }
	
     function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isCuentasCorrientes = isset($id) ? $id === self::GET_CUENTACORRIENTE : false;
        
        if($isCuentasCorrientes) {
            $idcuenta = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getCuentaCorriente($idcuenta);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->razonsocial, $obj->telefono, $obj->email, 
                $obj->direccion, $obj->idtipodecliente, $obj->cuit);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->razonsocial, $obj->telefono, $obj->email, 
                $obj->direccion, $obj->idtipodecliente, $obj->cuit);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}