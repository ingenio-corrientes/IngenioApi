<?php
/**
 * Description of ListaPrecioDB
 *
 * @author meza
 */
class ListaPrecioDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'listasprecios';
    
    public function getById($id=0){
        $query = "SELECT l.id, l.idtipocliente, l.idtiposervicio, 
                t.servicio, l.idlistabase, l.listaprecio, 
                l.desyreca, l.monto, l.doblefaz, l.color, l.fecultmodif 
            FROM listasprecios l
            LEFT JOIN tiposservicios t ON t.id = l.idtiposervicio 
            WHERE l.id=$id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getListasBase($tiposervicio=0){
        $query = "SELECT l.id, l.idtipocliente, l.idtiposervicio, 
                t.servicio, l.idlistabase, l.listaprecio, 
                l.desyreca, l.monto, l.doblefaz, l.color, l.fecultmodif 
            FROM listasprecios l
            LEFT JOIN tiposservicios t ON t.id = l.idtiposervicio 
            WHERE l.idtiposervicio = $tiposervicio;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getByIdPresupuesto($idpresupuesto, $tiposervicio) {
        $query = "SELECT l.* 
            FROM presupuestos p
            LEFT JOIN clientes c ON c.id = p.idcliente
            LEFT JOIN listasprecios l ON l.idtipocliente = c.idtipodecliente
            WHERE p.id = $idpresupuesto AND l.idtiposervicio = $tiposervicio
            ORDER BY l.fecultmodif DESC
            LIMIT 1;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $query = "SELECT l.id, l.idtipocliente, l.idtiposervicio, 
                t.servicio, l.idlistabase, l.listaprecio, 
                l.desyreca, l.monto, l.doblefaz, l.color, l.fecultmodif 
            FROM listasprecios l
            LEFT JOIN tiposservicios t ON t.id = l.idtiposervicio";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($idtipocliente='', $idtiposervicio='', $idlistabase='', 
            $listaprecio='', $desyreca='', $monto='',
            $doblefaz='', $color=''){
        
        $sp_insertnew = "";
        switch ($idtiposervicio) {
            case '1':
                $sp_insertnew = "CALL new_lpreciocarteleria";
                break;
            case '2':
                $sp_insertnew = "CALL new_lpreciodiseno";
                break;
            case '3':
                $sp_insertnew = "CALL new_lprecioimpresionlaser";
                break;
            case '4':
                $sp_insertnew = "CALL new_lpreciopapeleria";
                break;
        }

        $stmt = $this->mysqli->prepare("SET @o_newid:= -1");
        $stmt->execute();
        $sp_insertnew .= "($idtipocliente, $idtiposervicio, $idlistabase, 
                '$listaprecio', $desyreca, $monto, 
                $doblefaz, $color, @o_newid);";
//        var_dump($sp_insertnew);
//        return true;
        $r = $this->mysqli->query($sp_insertnew);
        $r = $this->mysqli->query('SELECT @o_newid as o_newid');
        $row = $r->fetch_assoc();

        return $row['o_newid'];
    }
    
    public function update($id=-1, 
            $idtipocliente='', $idtiposervicio='', $idlistabase='', 
            $listaprecio='', $desyreca='', $monto='',
            $doblefaz=0, $color=0) {
        $query = "UPDATE listasprecios SET 
                idtipocliente=$idtipocliente, idtiposervicio=$idtiposervicio, idlistabase=$idlistabase, 
                listaprecio='$listaprecio', desyreca=$desyreca, monto=$monto, 
                doblefaz=$doblefaz, color=$color, fecultmodif='NOW()' 
            WHERE id = $id;";
//        var_dump($query);
//        return true;
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function apply($idtiposervicio='', $listaprecio='', 
            $desyreca=0, $monto=0) {
        $sp_apply = "CALL apply_desc_rec_monto";

        $sp_apply .= "($listaprecio, $desyreca, $monto,
                $idtiposervicio);";

        $r = $this->mysqli->query($sp_apply);

        return $r;
//        $query = "";
//        switch ($idtiposervicio) {
//            case 1: // Cartelería
//                $query = "UPDATE lprecioscartelerias SET ";
//                break;
//            case 2: // Diseño
//                $query = "UPDATE lpreciosdisenos SET ";
//                break;
//            case 3: // Impresión Láser
//                $query = "UPDATE lpreciosimpresioneslaser SET ";
//                break;
//            case 4: // Papelería
//                $query = "UPDATE lpreciospapelerias SET ";
//                break;
//        }
//        if($tipo == 1) {// porcentaje
//            $query .= "precio = precio * (1 + ($desyreca) /100) ";
//        } else { // monto
//            $query .= "precio = precio + $monto ";
//        }
//        $query .= "WHERE idlistaprecio = $id;";
//
//        if($this->checkIntID(self::TABLE, $id)){
//            $stmt = $this->mysqli->prepare($query);
//            $r = $stmt->execute(); 
//            $stmt->close();
//            return $r;
//        }
//        return false;
    }

    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
