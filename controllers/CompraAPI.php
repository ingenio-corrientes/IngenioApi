<?php
/**
 * Description of CompraAPI
 *
 * @author meza
 */
class CompraAPI extends EntityAPI {
    const API_ACTION = 'compra';
    const POST_PAGAR = 'pagar';

    public function __construct() {
	$this->db = new CompraDB();
        $this->fields = [];
        array_push($this->fields, 
                'idproveedor',
                'idtipocomprobante',
                'numero',
                'fecemision',
                'observacion',
                'subtotal',
                'total');
    }
         
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        
        if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $id = filter_input(INPUT_GET, 'id');
        $isPagar = strpos($id, self::POST_PAGAR);
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->idproveedor, $obj->numero, $obj->fecemision,
                $obj->idtipocomprobante, $obj->observacion, $obj->subtotal,
                $obj->total);
        if($r) {
            $this->response(200,"success",$r);
        } else {
            $this->response(203,"error", self::ERROR_DUPLICATED);
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->update($id,
            $obj->idproveedor, $obj->numero, $obj->fecemision,
            $obj->idtipocomprobante, $obj->observacion, $obj->subtotal,
            $obj->total);
        
        if($r) { $this->response(200,"success", $r); }
        else { $this->response(204,"success","Record not updated");}
    }
}
