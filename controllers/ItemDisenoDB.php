<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemDiseñoDB
 *
 * @author meza
 */
class ItemDisenoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'itemsdisenos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT i.id, i.idtipodiseno, "
                . "i.idsubtipodiseno, i.descripcion, "
                . "i.cantidad, i.fecultmodif "
                . "FROM itemsdisenos i "
                . "LEFT JOIN tiposdisenos s ON i.idtipodiseno = t.id "
                . "LEFT JOIN subtiposdisenos t ON i.idsubtipodiseno = t.id");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idtipodiseno=-1, $idsubtipodiseno=-1, $descripcion='', 
            $cantidad=-1, $fecultmodif=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " ("
                . "idtipodiseno, idsubtipodiseno, descripcion, "
                . "cantidad, fecultmodif) "
                . "VALUES ("
                . "$idtipodiseno, $idsubtipodiseno, '$descripcion', "
                . "$cantidad, NOW());");
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update(
            $id=-1, $idtipodiseno=-1, $idsubtipodiseno=-1, 
            $descripcion='', $cantidad=-1, $fecultmodif='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET "
                    . "idtipodiseno= $idtipodiseno, idsubtipodiseno= $idsubtipodiseno, "
                    . "descripcion= '$descripcion', cantidad= $cantidad, "
                    . "fecultmodif= NOW() "
                    . "WHERE id = ?;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
