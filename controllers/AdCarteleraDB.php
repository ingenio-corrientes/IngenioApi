<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdCarteleraDB
 *
 * @author meza
 */
class AdCarteleraDB extends EntityDB{
   protected $mysqli;
   const TABLE = 'adicionalcarteleria';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT id, carteleria, anchomin, altomin, unidad, fecultmodif "
                . "FROM adicionalcarteleria ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $carteleria='', $anchomin=-1, 
            $altomin=-1, $unidad=-1, 
            $fecultmodif=''){
            $query= "INSERT INTO " . self::TABLE . " ("
                    . "carteleria, anchomin, "
                    . "altomin, unidad, "
                    . "fecultmodif) "
            . "VALUES ('$carteleria', $anchomin, $altomin, $unidad, NOW());";
            //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update(
            $id=-1, $carteleria='', 
            $anchomin=-1, $altomin=-1, 
            $unidad=-1) {
        if($this->checkIntID(self::TABLE, $id)){
            $query= "UPDATE " . self::TABLE . " SET "
                    . "carteleria= '$carteleria', anchomin= $anchomin, "
                    . "altomin= $altomin, unidad= $unidad, "
                    . "fecultmodif= NOW() "
                    . "WHERE id = $id;";
            //var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}
