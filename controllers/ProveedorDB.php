<?php
/**
 * Description of ProveedorDB
 *
 * @author meza
 */
class ProveedorDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'proveedores';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT p.id, p.razonsocial, p.cuit, p.telefono, p.email, p.direccion "
                . "FROM proveedores p");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }

    public function getCuentaCorrienteProveedor($idproveedor=-1){
//        $query = "SELECT x.* FROM 
//                (SELECT c.idproveedor, p.razonsocial, c.id as idcompra, c.numero, c.idtipocomprobante, t.tipocomprobante, c.fecemision, c.saldo, 
//                    (IFNULL(SUM(car.cantidad * car.precio + car.ancho * car.alto * car.precio), 0) + 
//                    IFNULL(SUM(dis.cantidad * dis.precio), 0) +
//                    IFNULL(SUM(imp.cantidad * imp.precio), 0) +
//                    IFNULL(SUM(pap.unidades * pap.precio), 0) +
//                    IFNULL(SUM(adc.unidades * adc.precio + adc.ancho * adc.alto * adc.precio), 0) +
//                    IFNULL(SUM(adp.unidades * adp.precio), 0)
//                    ) as monto
//                FROM compras c 
//                LEFT JOIN proveedores p ON p.id = c.idproveedor
//				LEFT JOIN tiposcomprobantes t oN t.id = c.idtipocomprobante
//                LEFT JOIN presupuestositemscarteleria car ON car.idpresupuesto = c.id
//                LEFT JOIN presupuestositemsdiseno dis ON dis.idpresupuesto = c.id
//                LEFT JOIN presupuestositemsimpresionlaser imp ON imp.idpresupuesto = c.id
//                LEFT JOIN presupuestositemspapeleria pap ON pap.idpresupuesto = c.id
//                LEFT JOIN presupuestositsadscarteleria adc ON adc.idpresitcarteleria = car.id
//                LEFT JOIN presupuestositsadspapeleria adp ON adp.idpresitpapeleria = pap.id
//                WHERE c.idproveedor = $idproveedor AND c.saldo > 0)x
//            WHERE x.saldo > 0;";
//        var_dump($query);
        $query = "SELECT c.idproveedor, p.razonsocial, c.id as idcompra, 
                c.numero, c.idtipocomprobante, t.tipocomprobante, c.fecemision, 
                c.saldo, c.total
            FROM compras c
            LEFT JOIN proveedores p ON p.id = c.idproveedor
            LEFT JOIN tiposcomprobantes t oN t.id = c.idtipocomprobante
            WHERE c.idproveedor = $idproveedor AND c.saldo > 0;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($razonsocial='', $cuit=-1, $telefono='', $email='', 
            $direccion=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (razonsocial, cuit, telefono, "
                . "email, direccion) "
                . "VALUES (?, ?, ?, ?, ?);");
        $stmt->bind_param('sisss', $razonsocial, $cuit, $telefono, $email, $direccion);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function update($id=-1, $razonsocial='', $cuit=-1, $telefono='', 
            $email='', $direccion='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET razonsocial=?, cuit=?, "
                    . "telefono=?, email=?, direccion=? "
                    . "WHERE id = ?;");
            $stmt->bind_param('sisssi', $razonsocial, $cuit, $telefono, $email, 
                    $direccion, $id);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
