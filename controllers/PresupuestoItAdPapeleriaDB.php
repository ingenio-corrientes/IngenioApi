<?php
/**
 * Description of PresupuestoItAdPapeleriaDB
 *
 * @author meza
 */
class PresupuestoItAdPapeleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestositsadspapeleria';
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idpresitpapeleria, i.idtipoadicional, 
                t.tipoadicional, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadspapeleria i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional 
                WHERE i.id=$id;";
        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT i.id, i.idpresitpapeleria, i.idtipoadicional, 
                    t.tipoadicional, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadspapeleria i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPresItPapeleria($idpresitpapeleria) {
        $query = "SELECT i.id, i.idpresitpapeleria, i.idtipoadicional, 
                    t.tipoadicional, i.unidades, i.precio, i.fecultmodif 
                FROM presupuestositsadspapeleria i 
                LEFT JOIN tiposadicionales t ON t.id = i.idtipoadicional 
                WHERE i.idpresitpapeleria = $idpresitpapeleria";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idpresitpapeleria, $idtipoadicional, $unidades, 
            $precio){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadpapeleria(0,-1,
                $idpresitpapeleria, $idtipoadicional, $unidades, 
                $precio, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
//        $stmt = $this->mysqli->prepare(
//                "INSERT INTO presupuestositsadspapeleria 
//                        (idpresitpapeleria, idtipoadicional, unidades, precio, fecultmodif) 
//                VALUES ($idpresitpapeleria, $idtipoadicional, $unidades, $precio, NOW());");
//        $r = $stmt->execute();
//        
//        $stmt->close();
//        return $r;
    }
    
    public function update($id=-1, 
            $idpresitpapeleria=-1, $idtipoadicional=-1, $unidades=0, 
            $precio=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadpapeleria(1, $id,
                $idpresitpapeleria, $idtipoadicional, $unidades, 
                $precio, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
//        $query = "UPDATE presupuestositsadspapeleria SET 
//                    idpresitpapeleria= $idpresitpapeleria, idtipoadicional= $idtipoadicional, 
//                    unidades= $unidades, precio=$precio, fecultmodif= NOW() 
//                WHERE id = $id;";
////        var_dump($query);
//        if($this->checkIntID(self::TABLE, $id)){
//            $stmt = $this->mysqli->prepare($query);
//            $r = $stmt->execute(); 
//            $stmt->close();
//            return $r;
//        }
//        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitadpapeleria(2, $id,
                0, 0, 0, 
                0, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return 0;
//        $stmt = $this->mysqli->prepare("DELETE FROM presupuestositsadspapeleria WHERE id = $id;");
//        $r = $stmt->execute(); 
//        $stmt->close();
//        return $r;
    }
}