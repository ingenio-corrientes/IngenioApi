<?php
/**
 * Description of ChequeDB
 *
 * @author meza
 */
class ChequeDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'cheques';
    
    public function getById($id=0){
        $query = "SELECT c.id, c.origen, c.idbanco, b.banco, 
                c.librador, c.numero, c.fecemision, c.fecvencimiento, c.monto, 
                c.depositado, c.cobrado, c.fecultmodif,
                (CASE c.origen
                        WHEN 0 THEN 'Recibo' 
                        WHEN 1 THEN 'Gasto'
                END) AS origenstr, 
                (CASE c.origen
                        WHEN 0 THEN l.razonsocial
                        WHEN 1 THEN 'Ingenio Corrientes' 
                END) AS tenedororiginal,
                (CASE c.origen
                    WHEN 0 THEN l.razonsocial 
                    WHEN 1 THEN 'Ingenio Corrientes' 
                END) AS entrega,
                (CASE c.origen
                        WHEN 0 THEN 'Ingenio Corrientes' 
                        WHEN 1 THEN p.razonsocial
                END) AS tenedoractual
            FROM cheques c 
            LEFT JOIN bancos b ON c.idbanco = b.id
            LEFT JOIN recibositems ri ON ri.idcheque = c.id
            LEFT JOIN gastositems gi ON gi.idcheque = c.id
            LEFT JOIN gastos g ON g.id = gi.idgasto
            LEFT JOIN recibos r ON r.id = ri.idrecibo
            LEFT JOIN proveedores p ON p.id = g.idproveedor
            LEFT JOIN clientes l ON l.id = r.idcliente
            WHERE c.id= $id;";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query ="SELECT c.id, c.origen, c.idbanco, b.banco, c.tenedororiginal, 
                c.librador, c.numero, c.fecemision, c.fecvencimiento, c.monto, 
                c.depositado, c.cobrado, c.fecultmodif, 
                (CASE c.origen
                    WHEN 0 THEN 'Recibo' 
                    WHEN 1 THEN 'Gasto'
                END) AS origenstr,
                (CASE c.origen
                    WHEN 0 THEN IFNULL(l.razonsocial, '') 
                    WHEN 1 THEN 'Ingenio Corrientes' 
                END) AS entrega,
                (CASE c.origen
                    WHEN 0 THEN 'Ingenio Corrientes' 
                    WHEN 1 THEN IFNULL(p.razonsocial, '')
                END) AS tenedoractual
            FROM cheques c 
            LEFT JOIN bancos b ON c.idbanco = b.id
            LEFT JOIN recibositems ri ON ri.idcheque = c.id
            LEFT JOIN gastositems gi ON gi.idcheque = c.id
            LEFT JOIN gastos g ON g.id = gi.idgasto
            LEFT JOIN recibos r ON r.id = ri.idrecibo
            LEFT JOIN proveedores p ON p.id = g.idproveedor
            LEFT JOIN clientes l ON l.id = r.idcliente
            GROUP BY c.id
            ORDER BY c.fecultmodif DESC;";
//    var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getEnCartera() {
        $query = "SELECT h.id, h.origen, h.idbanco, h.tenedororiginal, h.librador, 
                h.numero, h.fecemision, h.fecvencimiento, h.monto, h.depositado, 
                h.cobrado, b.banco
            FROM cheques h
            LEFT JOIN bancos b ON b.id = h.idbanco
            WHERE h.propio = 0 AND h.depositado = 0 AND h.cobrado = 0;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $origen=0, $idbanco=-1, $tenedororiginal='',
            $librador='', $numero='', $fecemision='', 
            $fecvencimiento='', $monto=-1, $depositado=-1, 
            $cobrado=0){
        $query= "INSERT INTO cheques 
                    (origen, idbanco, tenedororiginal,
                    librador, numero, fecemision, 
                    fecvencimiento, monto, depositado, 
                    cobrado) 
                VALUES 
                    ($origen, $idbanco, '$tenedororiginal',
                    '$librador', '$numero', '$fecemision',
                    '$fecvencimiento', $monto, $depositado, 
                    $cobrado);";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $origen=0, $idbanco=-1, $tenedororiginal='',
            $librador='', $numero='', $fecemision='', 
            $fecvencimiento='', $monto=-1, $depositado=-1, 
            $cobrado=0) {
        if($this->checkIntID(self::TABLE, $id)){
            $query= "UPDATE cheques SET 
                origen = $origen, idbanco = $idbanco, tenedororiginal='$tenedororiginal',
                librador = '$librador', numero = '$numero', fecemision ='$fecemision', 
                fecvencimiento = '$fecvencimiento', monto = $monto, depositado = $depositado, 
                cobrado = $cobrado
                WHERE id = $id;";
//            var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function setCobrado($id=-1, $cobrado=0) {
        if($this->checkIntID(self::TABLE, $id)){
            $query= "UPDATE cheques SET 
                cobrado = $cobrado, fecultmodif= NOW() 
                WHERE id = $id;";
            //var_dump($query);
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if ($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}