<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemImpresionLaserDB
 *
 * @author meza
 */
class ItemImpresionLaserDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'itemsimpresionlaser';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
            "SELECT i.id, i.idgramaje, i.idtamaño, i.descripcion, "
                . "i.cantidad, i.fecultmodif "
                . "FROM itemsimpresionlaser i "
                . "LEFT JOIN gramajes g ON i.idgramaje = t.id "
                . "LEFT JOIN tamaños t ON i.idtamaño = t.id");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idgramaje=-1, $idtamaño=-1, $descripcion='', 
            $cantidad=-1, $fecultmodif=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " ("
                . "idgramaje, idtamaño, descripcion, "
                . "cantidad, fecultmodif) "
                . "VALUES ("
                . "$idgramaje, $idtamaño, '$descripcion', "
                . "$cantidad, NOW());");
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update(
            $id=-1, $idgramaje=-1, $idtamaño=-1, 
            $descripcion='', $cantidad=-1, $fecultmodif='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET "
                    . "idgramaje= $idgramaje, idtamaño= $idtamaño, "
                    . "descripcion= '$descripcion', cantidad= $cantidad, "
                    . "fecultmodif= NOW() "
                    . "WHERE id = ?;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}
