<?php
/**
 * Description of PresupuestoItAdImpresionLaserAPI
 *
 * @author meza
 */
class PresupuestoItAdImpresionLaserAPI extends EntityAPI {
    const GET_BYIDPRESITIMPRESIONLASER = 'byidpresitimpresionlaser';
    const API_ACTION = 'presupuestoitadimpresionlaser';

    public function __construct() {
        $this->db = new PresupuestoItAdImpresionLaserDB();
        $this->fields = [];
        array_push($this->fields, 
                'idpresitimpresionlaser',
                'idtipoadicional',
                'unidades',
                'precio');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdpresitimpresionlaser = ($id == self::GET_BYIDPRESITIMPRESIONLASER);
//        var_dump($id);
//        exit;
        if($isByIdpresitimpresionlaser){
            $fld1 = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdPresItImpresionLaser($fld1);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idpresitimpresionlaser, $obj->idtipoadicional,
                $obj->unidades, $obj->precio);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idpresitimpresionlaser, $obj->idtipoadicional, 
                $obj->unidades, $obj->precio);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}