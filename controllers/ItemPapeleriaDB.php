<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ItemPapeleriaDB
 *
 * @author meza
 */
class ItemPapeleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'itemspapeleria';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT "
                . "i.id, i.idtipojobpapel, i.idgramaje, i.idtamaño, i.unidades, "
                . "i.idtipopapelcom, i.idsubpapecom, i.descripcion, i.idtipocorte, "
                . "i.pilegos, i.fecultmodif "
                . "FROM itemspapeleria i "
                . "LEFT JOIN tipospapeleria s ON i.idtipojobpapel = s.id "
                . "LEFT JOIN gramajes t ON i.idgramaje = t.id");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idtipojobpapel=-1, $idgramaje=-1, $idtamaño=-1, 
            $unidades=-1, $idtipopapelcom='', $idsubpapecom=-1, 
            $descripcion='', $idtipocorte=-1, $pilegos=-1, 
            $fecultmodif=''){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " ("
                . "idtipojobpapel, idgramaje, idtamaño, "
                . "unidades, idtipopapelcom, idsubpapecom, "
                . "descripcion, idtipocorte, pilegos, "
                . "fecultmodif) "
                . "VALUES ("
                . "$idtipojobpapel, $idgramaje, $idtamaño, "
                . "$unidades, $idtipopapelcom, $idsubpapecom, "
                . "'$descripcion', $idtipocorte, $pilegos, "
                . "NOW());");
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update(
            $id=-1, $idtipojobpapel=-1, $idgramaje=-1, 
            $idtamaño=-1, $unidades=-1, $idtipopapelcom=-1, 
            $idsubpapecom=-1, $descripcion='', $idtipocorte=-1, 
            $pilegos=-1, $fecultmodif='') {
    if($this->checkID($id)){
        $stmt = $this->mysqli->prepare(
        "UPDATE " . self::TABLE . " SET "
            . "idtipojobpapel= $idtipojobpapel, idgramaje= $idgramaje, idtamaño= $idtamaño, "
            . "unidades= $unidades, idtipopapelcom= $idtipopapelcom, idsubpapecom= $idsubpapecom, "
            . "descripcion= '$descripcion', idtipocorte= $idtipocorte, pilegos= $pilegos, "
            . "fecultmodif= NOW() "
            . "WHERE id = ?;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
        return false;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}