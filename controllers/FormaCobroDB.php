<?php
/**
 * Description of FormaCobroDB
 *
 * @author meza
 */
class FormaCobroDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'formascobro';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT id, formacobro, bancario, cheque, fecultmodif "
                . "FROM formascobro ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($formacobro='', $bancario, $cheque){
        $query= "INSERT INTO " . self::TABLE . " (formacobro, bancario, cheque, fecultmodif) "
        . "VALUES ('$formacobro', $bancario, $cheque, NOW());";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
    
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $formacobro='', $bancario, $cheque) {
        if($this->checkIntID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET formacobro= '$formacobro', bancario= $bancario, cheque=$cheque, fecultmodif= NOW() "
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}