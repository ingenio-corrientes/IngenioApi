<?php
/**
 * Description of UsuarioApi
 *
 * @author WebDev
 */
class UsuarioAPI extends EntityAPI {
    const API_ACTION = 'usuario';
    const GET_AUTHENTICATE = 'authenticate';
    const GET_DATE = 'date';

    public function __construct() {
	$this->db = new UsuarioDB();
    }
    function processGet(){
//        echo "capo";
        $id = filter_input(INPUT_GET, 'id');
        $isAuthenticate = isset($id) ? $id === self::GET_AUTHENTICATE : false;
        $isDate = isset($id) ? $id === self::GET_DATE : false;
        
        if ($isAuthenticate) {
            $usuario = filter_input(INPUT_GET, 'fld1');
            $contrasena = filter_input(INPUT_GET, 'fld2');
//            var_dump($contrasena);
            $r = $this->db->authenticate( $usuario,  $contrasena);
            if ($r) {
                $this->response(200,"success", $r . "");
            } else {
                $this->response(401,"unauthorized","No autorizado.");
            }
        } elseif ($isDate) {
          $datetime = new DateTime();
          $datetimedb = $this->db->getDBDatetime();
//          var_dump($datetime);
          var_dump($datetimedb);
          $this->response(200,"success", $datetime);
        } elseif ($id){
            $response = $this->db->getUsuario( $id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getUsuarios();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","No se enviaron datos.");
        } else if(isset($obj->auth)) {
            $response = $this->db->authenticate( $obj->auth->usuario,  $obj->auth->contrasena);
            if($response){
                $this->response(200,"success", $response[0]['id']);
            } else {
                $this->response(200,"error","Usuario o contraseña erróneos.");
            }
        } else if(isset($obj->apellido) AND isset($obj->nombre) 
                AND isset($obj->usuario) AND isset($obj->contrasena)
                AND isset($obj->idrol)) {
            $r = $this->db->insert($obj->apellido, $obj->nombre, 
                    $obj->usuario, $obj->contrasena, $obj->idrol);
            if($r) { $this->response(200,"success","Registro agregado con éxito."); }
        } else {
            $this->response(422,"error","Faltan datos obligatorios.");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if(!$id) {
            $this->response(422,"error","Id no enviado o erróneo.");
            exit;
        }        
        if (empty($objArr)){                        
            $this->response(422,"error","No se enviaron datos.");
            exit;
        }
        if(isset($obj->reset)){
            if($this->db->resetPass($id, $obj->contrasena)){
                $this->response(200,"success","Contraseña reseteada.");
            } else {
                $this->response(304,"success","Contraseña no reseteada.");
            }
            exit;
        }
        if(isset($obj->change)){
            if($this->db->changePass($id, $obj->newcontrasena)){
                $this->response(200,"success","Contraseña cambiada.");
            } else {
                $this->response(304,"success","Contraseña no cambiada.");
            }
            exit;
        }
        if(isset($obj->apellido) AND isset($obj->nombre)
          AND isset($obj->usuario) AND isset($obj->contrasena)){
            $r = $this->db->update($id, $obj->apellido, $obj->nombre, 
              $obj->usuario, $obj->contrasena, $obj->idrol);
            if($r){
                $this->response(200,"success","Registro actualizado.");
            } else {
                $this->response(304,"success","Registro no actualizado.");
            }
        }else{
            $this->response(422,"error","Faltan datos obligatorios.");
        }
    }
}