<?php
/**
 * Description of CompraDB
 *
 * @author meza
 */
class CompraDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'compras';
    
    public function getById($id=0){
        $query = "SELECT c.id, c.idproveedor, p.razonsocial AS proveedor, 
                c.idtipocomprobante, t.tipocomprobante, c.numero,
                c.fecemision, c.observacion, 
                c.subtotal, c.total, c.saldo, 
                IFNULL(c.subtotal, 0) - IFNULL(SUM(i.precio), 0) AS tope,
                CASE WHEN IFNULL(d.idgasto, 0) = 0 THEN 0 ELSE 1 END AS bloqueado
            FROM compras c 
            LEFT JOIN comprasitems i ON i.idcompra = c.id
            LEFT JOIN proveedores p ON p.id = c.idproveedor
            LEFT JOIN tiposcomprobantes t ON t.id = c.idtipocomprobante
            LEFT JOIN gastosdeudas d ON d.idcompra = c.id
            WHERE c.id='$id';";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT c.id, c.idproveedor, p.razonsocial AS proveedor, 
                c.idtipocomprobante, t.tipocomprobante, c.numero, 
                c.fecemision, c.observacion, 
                c.subtotal, c.total, c.saldo 
                FROM compras c 
                LEFT JOIN proveedores p ON p.id = c.idproveedor
                LEFT JOIN tiposcomprobantes t ON t.id = c.idtipocomprobante;";
       //var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idproveedor=-1, $numero='', $fecemision='',
            $idtipocomprobante=-1, $observacion='', $subtotal=0,
            $total=0){
        $id = self::gen_uuid();
        $saldo = $total;
        $query= "INSERT INTO compras (
                id,
                idproveedor, numero, fecemision,
                idtipocomprobante, observacion, subtotal,
                total, saldo, fecultmodif) 
            SELECT '$id',
                $idproveedor, '$numero', '$fecemision',
                $idtipocomprobante, '$observacion', $subtotal,
                $total, $saldo, NOW()
            FROM dual 
            WHERE NOT EXISTS (
                SELECT id 
                FROM compras 
                WHERE idtipocomprobante = $idtipocomprobante
                    AND numero = $numero AND idproveedor = $idproveedor) 
            LIMIT 1;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $cant = $stmt->affected_rows;
        $stmt->close();
//        var_dump($cant);
//        return true;
        if ($cant > 0)  {
            $lastid = $id;
        } else {
            $lastid = "";
        }
        return $lastid;
    }
    
    public function update($id='', 
            $idproveedor=-1, $numero='', $fecemision='',
            $idtipocomprobante=-1, $observacion='', $subtotal=0,
            $total=0) {
        
        if($this->checkStringID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                "UPDATE " . self::TABLE . " SET "
                . "idproveedor = $idproveedor, numero = $numero, fecemision = '$fecemision', "
                . "idtipocomprobante = $idtipocomprobante, observacion= '$observacion', subtotal= $subtotal, "
                . "total= $total, fecultmodif = NOW() "
                . "WHERE id = '$id';");
            $r = $stmt->execute(); 
            $stmt->close();
            return $id;
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = '$id';";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
	
//    public function delete($id='') {
//        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = '$id';");
//        $r = $stmt->execute(); 
//        $stmt->close();
//        return $r;
//    }
}
