<?php
/**
 * Description of PresupuestoItemCarteleriaDB
 *
 * @author meza
 */
class PresupuestoItemCarteleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestositemscarteleria';
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idpresupuesto, i.idsustrato, i.idsubtiposus, 
                    i.ancho, i.alto, i.descripcion, i.cantidad, i.precio, i.fecultmodif 
                    FROM presupuestositemscarteleria i 
                    LEFT JOIN sustratos s ON i.idsustrato = s.id 
                    LEFT JOIN subtipossustratos t ON i.idsubtiposus = t.id
                    WHERE i.id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT i.id, i.idpresupuesto, i.idsustrato, i.idsubtiposus, 
                    i.ancho, i.alto, i.descripcion, i.cantidad, i.precio, i.fecultmodif 
                    FROM presupuestositemscarteleria i 
                    LEFT JOIN sustratos s ON i.idsustrato = s.id 
                    LEFT JOIN subtipossustratos t ON i.idsubtiposus = t.id";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPresupuesto($idpresupuesto) {
        $query = "SELECT i.id, i.idpresupuesto, i.idsustrato, i.idsubtiposus, 
                i.ancho, i.alto, i.descripcion, i.cantidad, i.precio, i.fecultmodif 
                FROM presupuestositemscarteleria i 
                LEFT JOIN sustratos s ON i.idsustrato = s.id 
                LEFT JOIN subtipossustratos t ON i.idsubtiposus = t.id
                WHERE i.idpresupuesto = $idpresupuesto";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idpresupuesto=-1, $idsustrato=-1, $idsubtiposus=-1, 
            $ancho=-1, $alto=-1, $cantidad=0, 
            $descripcion='', $precio=0){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitcarteleria(0,-1,
                $idpresupuesto, $idsustrato, $idsubtiposus,
                $ancho, $alto, $cantidad, 
                '$descripcion', $precio, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idpresupuesto=-1, $idsustrato=-1, $idsubtiposus=-1, 
            $ancho=-1, $alto=-1, $cantidad=0, 
            $descripcion='', $precio=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitcarteleria(1,$id,
                $idpresupuesto, $idsustrato, $idsubtiposus,
                $ancho, $alto, $cantidad, 
                '$descripcion', $precio, @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitcarteleria(2,$id,
                0, 0, 0,
                0, 0, 0, 
                '', 0, @p_oresult);";
//        var_dump($query);
//        return 22;
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return 0;
    }
}