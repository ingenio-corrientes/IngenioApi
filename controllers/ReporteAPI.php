<?php
class ReporteAPI extends EntityAPI {
    const API_ACTION = 'reporte';
    const GET_RESUMEN = 'resumen';

    public function __construct() {
        $this->db = new ReporteDB();
//        $this->fields = [];
//        array_push($this->fields, 
//                'idcliente', 
//                'fecha', 
//                'idpresupuesto', 
//                'fecemision',  
//                'dirty');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isFilteredByDates = isset($id) ? $id === self::GET_RESUMEN : false;
        
        if ($isFilteredByDates) {
            $response = $this->db->getResumen();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    } 
}
