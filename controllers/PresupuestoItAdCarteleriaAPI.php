<?php
/**
 * Description of PresupuestoItAdCarteleriaAPI
 *
 * @author meza
 */
class PresupuestoItAdCarteleriaAPI extends EntityAPI {
    const GET_BYIDPRESITCARTELERIA = 'byidpresitcarteleria';
    const API_ACTION = 'presupuestoitadcarteleria';

    public function __construct() {
        $this->db = new PresupuestoItAdCarteleriaDB();
        $this->fields = [];
        array_push($this->fields, 
                'idpresitcarteleria',
                'idtipoadicional',
                'ancho',
                'alto',
                'unidades',
                'precio');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdpresitcarteleria = ($id == self::GET_BYIDPRESITCARTELERIA);
        
        if($isByIdpresitcarteleria){
            $fld1 = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdPresItCarteleria($fld1);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else if($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idpresitcarteleria, $obj->idtipoadicional, 
                $obj->ancho, $obj->alto, 
                $obj->unidades, $obj->precio);
        if($r) {$this->response(200,"success","new record added"); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        
        $r = $this->db->update($id,
                $obj->idpresitcarteleria, $obj->idtipoadicional, 
                $obj->ancho, $obj->alto, 
                $obj->unidades, $obj->precio);
        $r = true;
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}