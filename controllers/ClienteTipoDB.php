<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClienteTipoDB
 *
 * @author meza
 */
class ClienteTipoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'clientestipo';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT id, clientetipo, fecultmodif "
                . "FROM clientestipo ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($clientetipo=''){
        $query= "INSERT INTO " . self::TABLE . " (clientetipo, fecultmodif) "
        . "VALUES ('$clientetipo', NOW());";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $clientetipo='') {
        if($this->checkID($id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET clientetipo= '$clientetipo', fecultmodif= NOW() "
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        var_dump($query);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
     public function checkID($id){
        $stmt = $this->mysqli->prepare("SELECT * FROM " . self::TABLE 
                . " WHERE ID=?");
        $stmt->bind_param("i", $id);
        if($stmt->execute()){
            $stmt->store_result();    
            if ($stmt->num_rows == 1){                
                return true;
            }
        }        
        return false;
    }
}