<?php
/**
 * Description of SubTipoSustratoDB
 *
 * @author meza
 */
class SubTipoSustratoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'subtipossustratos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT t.id, t.idsustrato, s.sustrato, t.subtiposustrato, 
                    t.anchomin, t.altomin, t.anchomax, t.altomax 
                FROM subtipossustratos t
                LEFT JOIN sustratos s ON s.id = t.idsustrato");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idsustrato=-1, $subtiposustrato='', $anchomin=0, 
                $altomin=0, $anchomax=0, $altomax=0){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (idsustrato, subtiposustrato, "
                . "anchomin, altomin, anchomax, altomax) "
                . "VALUES ($idsustrato, '$subtiposustrato', $anchomin, "
                . "$altomin, $anchomax, $altomax);");
        $r = $stmt->execute();
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, $idsustrato=-1, $subtiposustrato='', $anchomin=0, 
                $altomin=0, $anchomax=0, $altomax=0) {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET "
                    . "idsustrato=$idsustrato, subtiposustrato='$subtiposustrato', "
                    . "anchomin= $anchomin, altomin= $altomin, "
                    . "anchomax= $anchomax, altomax= $altomax "
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $id;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}

