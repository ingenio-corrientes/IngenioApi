<?php
/**
 * Description of LPrecioPapeleriaDB
 *
 * @author meza
 */
class LPrecioPapeleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'lpreciospapelerias';
    
    public function getList(){
        $query = "SELECT l.id, l.idlistaprecio, l.idtipopapeleriatrabajo, t.tipotrabajopapeleria, 
                l.idgramaje, g.gramaje, 
                (CASE t.comercial WHEN 1 THEN c.id ELSE 1 END) AS idtipopapelcom, 
                (CASE t.comercial WHEN 1 THEN 
                    CONCAT(c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial)
                ELSE 
                    CONCAT(g.gramaje, '|', p.tipocorte)
                END) AS descripcion,
                c.tipopapeleriacomercial,
                l.idsubtipopapelcom, s.subtipopapeleriacomercial, 
                l.idtamano, m.tamano,
                l.idtipocorte, p.tipocorte,
                t.comercial, t.concorte,
                l.precio, lp.doblefaz, lp.color, l.fecultmodif
            FROM lpreciospapelerias l
            LEFT JOIN listasprecios lp ON lp.id = l.idlistaprecio
            LEFT JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo
            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom
            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial
            LEFT JOIN tamanos m ON m.id = l.idtamano
            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte;";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
//    public function getList(){
//        $query = "SELECT l.id, l.idlistaprecio, l.idtipopapeleriatrabajo, t.tipotrabajopapeleria, 
//                l.idgramaje, g.gramaje, 
//                (CASE t.comercial WHEN 1 THEN c.id ELSE 1 END) AS idtipopapelcom, 
//                (CASE t.comercial WHEN 1 THEN 
//                    CONCAT(c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial)
//                ELSE 
//                    CONCAT(g.gramaje, '|', p.tipocorte, '|', (CASE l.doblefaz WHEN 0 THEN 'Simple Faz' ELSE ' Doble Faz' END))
//                END) AS descripcion,
//                c.tipopapeleriacomercial,
//                l.idsubtipopapelcom, s.subtipopapeleriacomercial, 
//                l.idtamano, m.tamano,
//                l.idtipocorte, p.tipocorte,
//                t.comercial, t.concorte,
//                l.precio, l.doblefaz, l.fecultmodif
//            FROM lpreciospapelerias l
//            LEFT JOIN listasprecios lp OM lp.id = l.lpreciospapelerias
//            LEFT JOIN gramajes g ON g.id = l.idgramaje
//            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo
//            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom
//            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial
//            LEFT JOIN tamanos m ON m.id = l.idtamano
//            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte;";
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
//    }
    
    public function getById($id=0){
        $query = "SELECT l.id, l.idlistaprecio, l.idtipopapeleriatrabajo, t.tipotrabajopapeleria, 
                l.idgramaje, g.gramaje, 
                (CASE t.comercial WHEN 1 THEN c.id ELSE 1 END) AS idtipopapelcom, 
                (CASE t.comercial WHEN 1 THEN 
                    CONCAT(c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial)
                ELSE 
                    CONCAT(g.gramaje)
                END) AS descripcion,
                c.tipopapeleriacomercial,
                l.idsubtipopapelcom, s.subtipopapeleriacomercial, 
                l.idtamano, m.tamano,
                l.idtipocorte, p.tipocorte,
                t.comercial, t.concorte,
                l.precio, lp.doblefaz, lp.color, l.fecultmodif
            FROM lpreciospapelerias l
            LEFT JOIN listasprecios lp ON lp.id = l.idlistaprecio
            LEFT JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo
            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom
            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial
            LEFT JOIN tamanos m ON m.id = l.idtamano
            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte
            WHERE l.id=$id;";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
//    public function getById($id=0){
//        $query = "SELECT l.id, l.idlistaprecio, l.idtipopapeleriatrabajo, t.tipotrabajopapeleria, 
//                l.idgramaje, g.gramaje, 
//                (CASE t.comercial WHEN 1 THEN c.id ELSE 1 END) AS idtipopapelcom, 
//                (CASE t.comercial WHEN 1 THEN 
//                    CONCAT(c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial)
//                ELSE 
//                    CONCAT(g.gramaje, '|', p.tipocorte, '|', (CASE l.doblefaz WHEN 0 THEN 'Simple Faz' ELSE ' Doble Faz' END))
//                END) AS descripcion,
//                c.tipopapeleriacomercial,
//                l.idsubtipopapelcom, s.subtipopapeleriacomercial, 
//                l.idtamano, m.tamano,
//                l.idtipocorte, p.tipocorte,
//                t.comercial, t.concorte,
//                l.precio, l.doblefaz, l.fecultmodif
//            FROM lpreciospapelerias l
//            LEFT JOIN listasprecios lp OM lp.id = l.lpreciospapelerias
//            LEFT JOIN gramajes g ON g.id = l.idgramaje
//            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo
//            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom
//            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial
//            LEFT JOIN tamanos m ON m.id = l.idtamano
//            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte
//            WHERE l.id=$id;";
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
//    }
    
    public function getByIdLista($idlista) {
        $query = "SELECT l.id, l.idlistaprecio, l.idtipopapeleriatrabajo, t.tipotrabajopapeleria, 
                l.idgramaje, g.gramaje, 
                (CASE t.comercial WHEN 1 THEN c.id ELSE 1 END) AS idtipopapelcom, 
                (CASE t.comercial WHEN 1 THEN 
                    CONCAT(c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial)
                ELSE 
                    CONCAT(g.gramaje)
                END) AS descripcion,
                c.tipopapeleriacomercial,
                l.idsubtipopapelcom, s.subtipopapeleriacomercial, 
                l.idtamano, m.tamano,
                l.idtipocorte, -1,
                t.comercial, t.concorte,
                l.precio, lp.doblefaz, lp.color, l.fecultmodif
            FROM lpreciospapelerias l
            LEFT JOIN listasprecios lp ON lp.id = l.idlistaprecio
            LEFT JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo
            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom
            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial
            LEFT JOIN tamanos m ON m.id = l.idtamano
            WHERE l.idlistaprecio = $idlista";
//        var_dump($query);
//        return true;
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
//    public function getByIdLista($idlista) {
//        $query = "SELECT l.id, l.idlistaprecio, l.idtipopapeleriatrabajo, t.tipotrabajopapeleria, 
//                l.idgramaje, g.gramaje, 
//                (CASE t.comercial WHEN 1 THEN c.id ELSE 1 END) AS idtipopapelcom, 
//                (CASE t.comercial WHEN 1 THEN 
//                    CONCAT(c.tipopapeleriacomercial, '|', s.subtipopapeleriacomercial)
//                ELSE 
//                    CONCAT(g.gramaje, '|', p.tipocorte)
//                END) AS descripcion,
//                c.tipopapeleriacomercial,
//                l.idsubtipopapelcom, s.subtipopapeleriacomercial, 
//                l.idtamano, m.tamano,
//                l.idtipocorte, p.tipocorte,
//                t.comercial, t.concorte,
//                l.precio, lp.doblefaz, lp.color, l.fecultmodif
//            FROM lpreciospapelerias l
//            LEFT JOIN listasprecios lp ON lp.id = l.idlistaprecio
//            LEFT JOIN gramajes g ON g.id = l.idgramaje
//            LEFT JOIN tipospapeleriastrabajos t ON t.id = l.idtipopapeleriatrabajo
//            LEFT JOIN subtipospapeleriascomerciales s ON s.id = l.idsubtipopapelcom
//            LEFT JOIN tipospapeleriascomerciales c ON c.id = s.idtipopapeleriacomercial
//            LEFT JOIN tamanos m ON m.id = l.idtamano
//            LEFT JOIN tipospapeleriascortes p ON p.id = l.idtipocorte
//            WHERE l.idlistaprecio = $idlista";
////        var_dump($query);
////        return true;
//        $result = $this->mysqli->query($query);
//        $entity = $result->fetch_all(MYSQLI_ASSOC);
//        $result->close();
//        return $entity;
//    }
    
    public function insert(
            $idlistaprecio, $idtipopapeleriatrabajo, $idgramaje, 
            $idsubtipopapelcom, $idtamano, $idtipocorte, 
            $precio){
        $query = "INSERT INTO " . self::TABLE . " "
                . "(idlistaprecio, idtipopapeleriatrabajo, idgramaje, "
                . "idsubtipopapelcom, idtamano, idtipocorte, "
                . "precio, fecultmodif) "
                . "VALUES "
                . "($idlistaprecio, $idtipopapeleriatrabajo, $idgramaje, 
                $idsubtipopapelcom, $idtamano, $idtipocorte, 
                $precio, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id, 
            $idlistaprecio, $idtipopapeleriatrabajo, $idgramaje, 
            $idsubtipopapelcom, $idtamano, $idtipocorte, 
            $precio) {
        $query = "UPDATE " . self::TABLE . " SET "
                . "idlistaprecio=$idlistaprecio, idtipopapeleriatrabajo=$idtipopapeleriatrabajo, "
                . "idgramaje=$idgramaje, idsubtipopapelcom=$idsubtipopapelcom, "
                . "idtamano=$idtamano, idtipocorte=$idtipocorte,"
                . "precio=$precio, fecultmodif=NOW() "
                . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function updateLista($id=-1) {
        $sp_insertnew = "CALL update_lpreciospapelerias($id);";
        $r = $this->mysqli->query($sp_insertnew);
        return $this->mysqli->errno;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}