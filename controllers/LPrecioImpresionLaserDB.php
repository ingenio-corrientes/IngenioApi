<?php
/**
 * Description of LPrecioImpresionLaserDB
 *
 * @author meza
 */
class LPrecioImpresionLaserDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'lpreciosimpresioneslaser';
    
    public function getList(){
        $query = "SELECT l.id, l.idlistaprecio, 
                l.idgramaje, g.gramaje, l.idtamano, t.tamano,
                CONCAT(g.gramaje, '|', t.tamano) AS descripcion,
                l.precio, lp.doblefaz, lp.color, l.redondear,  l.fecultmodif
            FROM lpreciosimpresioneslaser l
            LEFT JOIN listasprecios lp ON lp.id = l.idlistaprecio
            LEFt JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tamanos t ON t.id = l.idtamano;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getById($id=0){
        $query = "SELECT l.id, l.idlistaprecio, 
                l.idgramaje, g.gramaje, l.idtamano, t.tamano, 
                CONCAT(g.gramaje, '|', t.tamano) AS descripcion,
                l.precio, lp.doblefaz, lp.color, l.redondear, l.fecultmodif
            FROM lpreciosimpresioneslaser l
            LEFT JOIN listasprecios lp ON lp.id = l.idlistaprecio
            LEFt JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tamanos t ON t.id = l.idtamano
            WHERE l.id=$id;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdLista($idlista) {
        $query = "SELECT l.id, l.idlistaprecio, 
                l.idgramaje, g.gramaje, l.idtamano, t.tamano,
                CONCAT(g.gramaje, '|', t.tamano) AS descripcion,
                l.precio, lp.doblefaz, lp.color, l.redondear, l.fecultmodif
            FROM lpreciosimpresioneslaser l
            LEFT JOIN listasprecios lp ON lp.id = l.idlistaprecio
            LEFt JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tamanos t ON t.id = l.idtamano
            WHERE l.idlistaprecio = $idlista";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idlistaprecio=0, $idgramaje=0, $idtamano=0, 
            $precio=0, $redondear=0){
        $query = "INSERT INTO lpreciosimpresioneslaser
                (idlistaprecio, idgramaje, idtamano,
                precio, redondear, fecultmodif)
            VALUES 
                ($idlistaprecio, $idgramaje, $idtamano,
                $precio, $redondear, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idlistaprecio=-1, $idgramaje=-1, $idtamano=-1, 
            $precio=-1, $redondear=0) {
        $query = "UPDATE lpreciosimpresioneslaser SET 
                idlistaprecio=$idlistaprecio, idgramaje=$idgramaje, idtamano=$idtamano, 
                precio=$precio, redondear=$redondear, fecultmodif=NOW() 
            WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function updateLista($id=-1) {
        $sp_insertnew = "CALL update_lpreciosimpresionlaser($id);";
//        var_dump($sp_insertnew);
//        return true;
        $r = $this->mysqli->query($sp_insertnew);
        return $this->mysqli->errno;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}