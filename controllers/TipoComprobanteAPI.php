<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TipoComprobanteAPI
 *
 * @author meza
 */
class TipoComprobanteAPI extends EntityAPI {
    const API_ACTION = 'tipocomprobante';

    public function __construct() {
	$this->db = new TipoComprobanteDB();
        $this->fields = [];
        array_push($this->fields, 
                'tipocomprobante');
    }
         
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        
        if(isset($id)){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert(
                $obj->tipocomprobante);
        if($r) {$this->response(200,"success",$r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->update($id,
            $obj->tipocomprobante);
        
        if($r) { $this->response(200,"success", $r); }
        else { $this->response(204,"success","Record not updated");}
    }
}