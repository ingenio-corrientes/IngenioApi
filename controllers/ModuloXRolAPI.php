<?php
/**
 * Description of ModuloXRolAPI
 *
 * @author meza
 */
class ModuloXRolAPI extends EntityAPI {
    const GET_LISTBYIDROL = 'byidrol';
    const GET_LISTBYIDUSUARIOIDMODULO = 'byidusuarioidmodulo';
    const API_ACTION = 'moduloxrol';

    public function __construct() {
	$this->db = new ModuloXRolDB();
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isByIdRol = isset($id) ? $id === self::GET_LISTBYIDROL : false;
        $isByIdUsuarioIdModulo = isset($id) ? $id === self::GET_LISTBYIDUSUARIOIDMODULO : false;
        if($isByIdRol) {
            $idrol = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getByIdRol($idrol);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isByIdUsuarioIdModulo) {
            $idrol = filter_input(INPUT_GET, 'fld1');
            $idmodulo = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getByIdUsuarioIdModulo($idrol, $idmodulo);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
        } elseif (isset($obj->idrol) AND isset($obj->idmodulo) 
                AND isset($obj->ver) AND isset($obj->editar) 
                AND isset($obj->eliminar)) {
            $r = $this->db->insert(
                    $obj->idrol, $obj->idmodulo, 
                    $obj->ver, $obj->editar, 
                    $obj->eliminar);
            if($r) { $this->response(200,"success","new record added"); }
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id){
            $this->response(400, "error", "The id not defined");
            exit;
        }
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)){
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(isset($obj->idrol) AND isset($obj->idmodulo) 
                AND isset($obj->ver) AND isset($obj->editar) 
                AND isset($obj->eliminar)) {
            if($this->db->update($id, 
                    $obj->idrol, $obj->idmodulo, 
                    $obj->ver, $obj->editar, 
                    $obj->eliminar)) {
                $this->response(200,"success","Record updated");
            } else { $this->response(304,"success","Record not updated"); }
        }else{
            $this->response(422,"error","The property is not defined");
        }
    }
}
