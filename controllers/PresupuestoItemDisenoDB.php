<?php
/**
 * Description of PresupuestoItemDiseno
 *
 * @author meza
 */
class PresupuestoItemDisenoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestositemsdiseno';
    
    public function getById($id=0){
        $query = "SELECT * FROM " . self::TABLE . " WHERE id=$id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT i.id, i.idpresupuesto, i.idtipodiseno, i.idsubtipodiseno, 
                i.descripcion, i.cantidad, i.precio, i.fecultmodif 
                FROM presupuestositemsdiseno i 
                LEFT JOIN tiposdisenos d ON i.idtipodiseno = d.id 
                LEFT JOIN subtiposdisenos t ON i.idsubtipodiseno = t.id";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdPresupuesto($idpresupuesto) {
        $query = "SELECT i.id, i.idpresupuesto, i.idtipodiseno, i.idsubtipodiseno, 
                i.descripcion, i.cantidad, i.precio, i.fecultmodif 
                FROM presupuestositemsdiseno i 
                LEFT JOIN tiposdisenos d ON i.idtipodiseno = d.id 
                LEFT JOIN subtiposdisenos t ON i.idsubtipodiseno = t.id
                WHERE i.idpresupuesto = $idpresupuesto";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idpresupuesto=-1, $idtipodiseno=-1, $idsubtipodiseno=-1, 
            $descripcion='', $cantidad=0, $precio=0){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitdiseno(0,-1,
                $idpresupuesto, $idtipodiseno, $idsubtipodiseno,
                '$descripcion', $cantidad, $precio, 
                @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idpresupuesto=-1, $idtipodiseno=-1, $idsubtipodiseno=-1, 
            $descripcion='', $cantidad=0, $precio=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitdiseno(1,$id,
                $idpresupuesto, $idtipodiseno, $idsubtipodiseno,
                '$descripcion', $cantidad, $precio,
                 @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return $row['p_oresult'];
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_presitdiseno(2, $id,
                0, 0, 0, 
                '', 0, 0,
                @p_oresult);";
        $r = $this->mysqli->query($query);
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        if ($row['p_oresult'] === "-1") {
            return false;
        }
        return 0;
    }
}