<?php
/**
 * Description of TipoTrabajoPapeleriaDB
 *
 * @author meza
 */
class TipoTrabajoPapeleriaDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tipospapeleriastrabajos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $query = "SELECT * FROM ". self::TABLE;
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($tipotrabajopapeleria, $comercial, $concorte){
        $query = "INSERT INTO tipospapeleriastrabajos 
                (tipotrabajopapeleria, comercial, concorte) 
            VALUES 
                ('$tipotrabajopapeleria', $comercial, $concorte);";
        $stmt = $this->mysqli->prepare($query );
        $r = $stmt->execute();
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $tipotrabajopapeleria, $comercial, $concorte) {
        $query = "UPDATE tipospapeleriastrabajos SET 
                tipotrabajopapeleria = '$tipotrabajopapeleria', comercial = $comercial, concorte = $concorte
            WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }	
}
