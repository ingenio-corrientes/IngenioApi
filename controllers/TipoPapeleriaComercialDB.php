<?php
/**
 * Description of TipoPapeleriaComercialDB
 *
 * @author meza
 */
class TipoPapeleriaComercialDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tipospapeleriascomerciales';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM ". self::TABLE);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($tipopapeleriacomercial=""){
        $query = "INSERT INTO " . self::TABLE . " (tipopapeleriacomercial) "
                . "VALUES ('$tipopapeleriacomercial');";
//        var_dump($query);
//        return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, $tipopapeleriacomercial="") {
        $query="UPDATE " . self::TABLE . " SET tipopapeleriacomercial = '$tipopapeleriacomercial' "
                    . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
