<?php
/**
 * Description of PresupuestoDB
 *
 * @author meza
 */
class PresupuestoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'presupuestos';
    
    public function getById($id=0){
        $query = "SELECT p.id, p.idusuario, u.usuario, p.idcliente, 
                c.razonsocial, p.idformacobro, f.formacobro, 
                IFNULL(p.fecemision, '') AS fecemision, p.observacion, 
                p.descuyrecar, IFNULL(p.fecentrega, '') AS fecentrega, 
                p.fecultmodif, IFNULL(p.fecaproduccion, '') AS fecaproduccion, 
                p.total, p.saldo, p.actacte, p.dirty,
                (CASE WHEN fecentrega IS NOT NULL THEN 'Entregado'
                    ELSE CASE WHEN fecaproduccion IS NOT NULL THEN 'En Producción'
                    ELSE CASE WHEN fecemision IS NOT NULL THEN 'Emitido'
                    ELSE 'Borrador' END END END) AS estado
            FROM presupuestos p
            LEFT JOIN usuarios u ON p.idusuario = u.id 
            LEFT JOIN clientes c ON p.idcliente = c.id 
            LEFT JOIN formascobro f ON p.idformacobro = f.id
            WHERE p.id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT p.id, p.idusuario, u.usuario, p.idcliente, c.razonsocial, 
                p.idformacobro, f.formacobro, IFNULL(p.fecemision, '') AS fecemision, p.observacion, 
                p.descuyrecar, IFNULL(p.fecentrega, '') AS fecentrega, p.fecultmodif, IFNULL(p.fecaproduccion, '') AS fecaproduccion, 
                p.total, p.dirty, p.saldo, p.actacte,
                (CASE WHEN p.fecentrega IS NOT NULL THEN 'Recibo Generado'
                    ELSE CASE WHEN p.fecaproduccion IS NOT NULL THEN 'En Producción'
                    ELSE CASE WHEN p.fecemision IS NOT NULL THEN 'Emitido'
                    ELSE 'Borrador' END END END) AS estado
            FROM presupuestos p 
            LEFT JOIN usuarios u ON p.idusuario = u.id 
            LEFT JOIN clientes c ON p.idcliente = c.id 
            LEFT JOIN formascobro f ON p.idformacobro = f.id;";
//        var_dump($query);
        // $result = $this->mysqli->query($query);
        // $entity = $result->fetch_all(MYSQLI_ASSOC);
        // $result->close();
        // return $entity;
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getPendientesByIdCliente($idcliente=0){
        $query = "SELECT p.id, p.idusuario, u.usuario, p.idcliente, c.razonsocial, 
                p.idformacobro, f.formacobro, IFNULL(p.fecemision, '') AS fecemision, p.observacion, 
                p.descuyrecar, IFNULL(p.fecentrega, '') AS fecentrega, p.fecultmodif, IFNULL(p.fecaproduccion, '') AS fecaproduccion, 
                p.total, p.saldo, p.actacte, p.dirty 
            FROM presupuestos p 
            LEFT JOIN usuarios u ON p.idusuario = u.id 
            LEFT JOIN clientes c ON p.idcliente = c.id 
            LEFT JOIN formascobro f ON p.idformacobro = f.id 
            WHERE p.idcliente = $idcliente AND p.saldo > 0;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getItemsById($id=0){
        $query = "SELECT ic.id, 1 as idtiposervicio, 'Cartelería' AS tiposervicio, 
                ic.descripcion, RPAD(ic.descripcion, 70,' ') AS descripcionpdf, ic.cantidad, 
                ic.precio, (ic.precio * (ic.alto * ic.ancho) * ic.cantidad) AS subtotal
            FROM presupuestos p
            JOIN presupuestositemscarteleria ic ON ic.idpresupuesto = p.id
            WHERE p.id = $id
            UNION
            SELECT id.id, 2 as idtiposervicio, 'Diseño' AS tiposervicio, 
                id.descripcion, RPAD(id.descripcion, 70,' ') AS descripcionpdf, id.cantidad, 
                id.precio, (id.cantidad * id.precio) AS subtotal
            FROM presupuestos p
            JOIN presupuestositemsdiseno id ON id.idpresupuesto = p.id
            WHERE p.id = $id
            UNION
            SELECT ii.id, 3 as idtiposervicio, 'Impresión Láser' AS tiposervicio, 
                ii.descripcion, RPAD(ii.descripcion, 70,' ') AS descripcionpdf, ii.cantidad, 
                ii.precio, (ii.precio * ii.cantidad) AS subtotal
            FROM presupuestos p
            JOIN presupuestositemsimpresionlaser ii ON ii.idpresupuesto = p.id
            WHERE p.id = $id
            UNION
            SELECT ip.id, 4 as idtiposervicio, 'Papeleria' AS tiposervicio, 
                ip.descripcion, RPAD(ip.descripcion, 70,' ') AS descripcionpdf, ip.unidades AS cantidad, 
                ip.precio, (ip.precio * ip.unidades) AS subtotal
            FROM presupuestos p
            JOIN presupuestositemspapeleria ip ON ip.idpresupuesto = p.id
            WHERE p.id = $id";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getItemsAdsById($id=0){
        $query = "SELECT iac.id, 1 as idtiposervicio, 'Cartelería' AS tiposervicio, 
                iac.idpresitcarteleria AS idservicio, RPAD(ta.tipoadicional, 70, ' ') AS descripcion, ic.cantidad, 
                iac.precio, ((iac.precio * (iac.ancho * iac.alto)) + (iac.unidades * iac.precio)) AS subtotal
            FROM presupuestositsadscarteleria iac
            LEFT JOIN presupuestositemscarteleria ic ON ic.id = iac.idpresitcarteleria
            LEFT JOIN presupuestos p ON p.id = ic.idpresupuesto
            LEFT JOIN tiposadicionales ta ON ta.id = iac.idtipoadicional
            WHERE p.id = $id
            UNION
            SELECT iap.id, 4 as idtiposervicio, 'Papeleria' AS tiposervicio, 
                iap.idpresitpapeleria AS idservicio, RPAD(ta.tipoadicional, 70, ' ') AS descripcion, iap.unidades AS cantidad, 
                iap.precio, (iap.precio * iap.unidades) AS subtotal
            FROM presupuestositsadspapeleria iap 
            LEFT JOIN presupuestositemspapeleria ip ON ip.id = iap.idpresitpapeleria            
            LEFT JOIN presupuestos p ON p.id = ip.idpresupuesto
            LEFT JOIN tiposadicionales ta ON ta.id = iap.idtipoadicional
            WHERE p.id = $id
            UNION
            SELECT ial.id, 3 as idtiposervicio, 'Impresión Láser' AS tiposervicio, 
                ial.idpresitimpresionlaser AS idservicio, RPAD(ta.tipoadicional, 70, ' ') AS descripcion, ial.unidades AS cantidad, 
                ial.precio, (ial.precio * ial.unidades) AS subtotal
            FROM presupuestositsadsimpresionlaser ial 
            LEFT JOIN presupuestositemsimpresionlaser ip ON ip.id = ial.idpresitimpresionlaser 
            LEFT JOIN presupuestos p ON p.id = ip.idpresupuesto
            LEFT JOIN tiposadicionales ta ON ta.id = ial.idtipoadicional
            WHERE p.id = $id";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert($idusuario=-1, $idcliente=-1, $idformacobro=-1, 
            $fecemision=null, $observacion='', $descuyrecar=0, 
            $fecentrega=null, $fecaproduccion=null, $saldo=0, 
            $actacte=0, $dirty=0){
        $query = "INSERT INTO presupuestos 
                (idusuario, idcliente, idformacobro, 
                fecemision, observacion, descuyrecar, 
                fecentrega, fecaproduccion, saldo, 
                actacte, dirty, fecultmodif) 
            VALUES 
                ($idusuario, $idcliente, $idformacobro, 
                null, '$observacion', $descuyrecar, 
                null, null, $saldo, 
                $actacte, $dirty, NOW());";
        // var_dump($query);
        // return true;
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idusuario=-1, $idcliente=-1, $idformacobro=-1, 
            $fecemision=null, $observacion='', $descuyrecar=-1, 
            $fecentrega=null, $fecaproduccion=null, $saldo=0,
            $actacte=0, $dirty='') {
        
        $fecemision=isset($fecemision) ? 'NULL' : "'$fecemision'";
        $fecentrega=isset($fecentrega) ? 'NULL' : "'$fecentrega'";
        $fecaproduccion=isset($fecaproduccion) ? 'NULL' : "'$fecaproduccion'";
        $query = "UPDATE presupuestos SET 
                idusuario= $idusuario, idcliente= $idcliente, idformacobro= $idformacobro, 
                fecemision= $fecemision, observacion= '$observacion', descuyrecar= $descuyrecar,  
                fecentrega= $fecentrega, fecaproduccion= $fecaproduccion, saldo= $saldo, 
                actacte=$actacte, dirty= $dirty, fecultmodif= NOW() 
            WHERE id = $id;"; 
        // var_dump($query);
        // return true;
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function emitir($id=-1) {
        $query = "UPDATE presupuestos SET
                fecemision= NOW(), fecultmodif= NOW() 
            WHERE id = $id;"; 
//        var_dump($query);
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function aProduccion($id=-1) {
        $query = "UPDATE presupuestos
            JOIN (SELECT  
                    (IFNULL(SUM(car.cantidad * car.precio + car.ancho * car.alto * car.precio), 0) + 
                    IFNULL(SUM(dis.cantidad * dis.precio), 0) +
                    IFNULL(SUM(imp.cantidad * imp.precio), 0) +
                    IFNULL(SUM(pap.unidades * pap.precio), 0) +
                    IFNULL(SUM(adc.unidades * adc.precio + adc.ancho * adc.alto * adc.precio), 0) +
                    IFNULL(SUM(adp.unidades * adp.precio), 0)) as monto
                FROM presupuestos p
                LEFT JOIN presupuestositemscarteleria car ON car.idpresupuesto = p.id
                LEFT JOIN presupuestositemsdiseno dis ON dis.idpresupuesto = p.id
                LEFT JOIN presupuestositemsimpresionlaser imp ON imp.idpresupuesto = p.id
                LEFT JOIN presupuestositemspapeleria pap ON pap.idpresupuesto = p.id
                LEFT JOIN presupuestositsadscarteleria adc ON adc.idpresitcarteleria = car.id
                LEFT JOIN presupuestositsadspapeleria adp ON adp.idpresitpapeleria = pap.id
                WHERE p.id = $id) m
            SET
                saldo = m.monto,
                fecaproduccion = NOW(), fecultmodif = NOW()
            WHERE id = $id;"; 
//        var_dump($query);
//        return true;
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function aplicarDescRec($id=-1, $descuyrecar=0) {
        $query = "UPDATE presupuestos p SET
                p.descuyrecar = $descuyrecar,
		p.total = 
                    ((SELECT IFNULL(SUM(i.ancho * i.alto * i.cantidad * i.precio), 0)
                    FROM presupuestositemscarteleria i 
                    WHERE i.idpresupuesto = $id) + 
                    (SELECT IFNULL(SUM(i.cantidad * i.precio), 0)
                    FROM presupuestositemsdiseno i 
                    WHERE i.idpresupuesto = $id) + 
                    (SELECT IFNULL(SUM(i.cantidad * i.precio), 0)
                    FROM presupuestositemsimpresionlaser i 
                    WHERE i.idpresupuesto = $id) + 
                    (SELECT IFNULL(SUM(i.unidades * i.precio * i.pliegos), 0)
                    FROM presupuestositemspapeleria i 
                    WHERE i.idpresupuesto = $id) + 
                    (SELECT IFNULL(SUM(a.ancho * a.alto * a.unidades * a.precio), 0)
                    FROM presupuestositsadscarteleria a 
                    LEFT JOIN presupuestositemscarteleria i ON i.id = a.idpresitcarteleria
                    WHERE i.idpresupuesto = $id) + 
                    (SELECT IFNULL(SUM(a.unidades * a.precio), 0)
                    FROM presupuestositsadspapeleria a 
                    LEFT JOIN presupuestositemspapeleria i ON i.id = a.idpresitpapeleria
                    WHERE i.idpresupuesto = $id))
                    * (1 + $descuyrecar / 100)
            WHERE p.id = $id;"; 
//        var_dump($query);
//        return true;
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
   
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}