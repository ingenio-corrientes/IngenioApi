<?php
/**
 * Description of LPrecioImpresionLaserIntervaloDB
 *
 * @author meza
 */
class LPrecioImpresionLaserIntervaloDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'lpreciosimpresioneslaserintervalos';
    
    public function getList(){
        $query = "SELECT i.id, i.idlprecioimpresionlaser, l.precio AS preciobase, 
                i.maximo, i.porcentaje, l.precio * ( 1 + i.porcentaje / 100 ) AS precio,
                CONCAT(g.gramaje, '|', m.tamano) AS descripcion 
            FROM lpreciosimpresioneslaserintervalos i 
            LEFT JOIN lpreciosimpresioneslaser l ON l.id = i.idlprecioimpresionlaser
            LEFT JOIN gramajes g ON g.id = l.idgramaje 
            LEFT JOIN tamanos m ON m.id = l.idtamano 
            ORDER BY i.idlprecioimpresionlaser, i.maximo;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getById($id=0){
        $query = "SELECT i.id, i.idlprecioimpresionlaser, l.precio AS preciobase, 
                i.maximo, i.porcentaje, l.precio * ( 1 + i.porcentaje / 100 ) AS precio, 
                CONCAT(g.gramaje, '|', m.tamano) AS descripcion 
            FROM lpreciosimpresioneslaserintervalos i 
            LEFT JOIN lpreciosimpresioneslaser l ON l.id = i.idlprecioimpresionlaser 
            LEFT JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tamanos m ON m.id = l.idtamano 
            WHERE l.id=$id 
            ORDER BY i.idlprecioimpresionlaser, i.maximo;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdLista($idlista) {
        $query = "SELECT i.id, i.idlprecioimpresionlaser, l.precio AS preciobase, 
                i.maximo, i.porcentaje, 
                (CASE l.redondear WHEN 1
                THEN ROUND(l.precio * (1 + i.porcentaje / 100))
                ELSE l.precio * (1 + i.porcentaje / 100) 
                END) AS precio, 
                CONCAT(g.gramaje, '|', m.tamano) AS descripcion 
            FROM lpreciosimpresioneslaserintervalos i 
            LEFT JOIN lpreciosimpresioneslaser l ON l.id = i.idlprecioimpresionlaser 
            LEFT JOIN gramajes g ON g.id = l.idgramaje
            LEFT JOIN tamanos m ON m.id = l.idtamano 
            WHERE i.idlprecioimpresionlaser = $idlista 
            ORDER BY i.idlprecioimpresionlaser, i.maximo;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert(
            $idlprecioimpresionlaser = 0, $maximo = 0, $porcentaje = 0){
        $query = "INSERT INTO " . self::TABLE . " "
                . "(idlprecioimpresionlaser, maximo, porcentaje, fecultmodif) "
                . "VALUES "
                . "($idlprecioimpresionlaser, $maximo, $porcentaje, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idlprecioimpresionlaser=0, $maximo=0, $porcentaje=0) {
        $query = "UPDATE " . self::TABLE . " SET "
                . "idlprecioimpresionlaser=$idlprecioimpresionlaser, maximo=$maximo, "
                . "porcentaje=$porcentaje, fecultmodif=NOW() "
                . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function roundAmount($id=-1) {
        $sp_insertnew = "CALL update_lprecioscartelerias($id);";
        $r = $this->mysqli->query($sp_insertnew);
        return $this->mysqli->errno;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}