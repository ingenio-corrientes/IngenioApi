<?php
/**
 * Description of UsuarioDb
 *
 * @author WebDev
 */
class UsuarioDB extends EntityDB {
   const TABLE = 'usuarios';
   protected $mysqli;
    
    public function getUsuario($id=0){
        $query = "SELECT u.id, u.usuario, u.contrasena, 
                u.apellido, u.nombre, u.idrol, r.rol
            FROM usuarios u
            LEFT JOIN roles r ON u.idrol = r.id
            WHERE u.id = $id;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getUsuarios(){
        $query = "SELECT u.id, u.usuario, u.contrasena, 
                u.apellido, u.nombre, u.idrol, r.rol
            FROM usuarios u
            LEFT JOIN roles r ON u.idrol = r.id;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($apellido='', $nombre='', $usuario='', $contrasena='', $idrol=-1){
        $query = "INSERT INTO " . self::TABLE . " (apellido, nombre, usuario, contrasena, idrol, fecultmodif) "
                . "VALUES ('$apellido', '$nombre', '$usuario', '$contrasena', '$idrol', NOW());";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id, $apellido, $nombre, 
            $usuario, $contrasena, $idrol) {
        $query = "UPDATE usuarios SET apellido='$apellido', nombre='$nombre', "
                . "usuario='$usuario', contrasena='$contrasena', idrol=$idrol  WHERE id = $id;";
//        var_dump($query);
        if($this->checkIntID( self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function resetPass($id, $contrasena) {        
        $query = "UPDATE usuarios SET contrasena = '$contrasena' WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function changePass($id, $newcontrasena) {        
        $query = "UPDATE usuarios SET contrasena = '$newcontrasena' WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;    
        }
        return false;
    }
    
    public function authenticate($usuario='', $contrasena=''){
        $query = "SELECT id FROM usuarios WHERE usuario='$usuario' AND contrasena='$contrasena'";
//               var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $usuario = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        
        return $usuario;
    }
    
    public function getDBDatetime() {
        $query = "SELECT NOW() FROM dual";
//               var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $usuario = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        
        return $usuario;
    }
    
    /*
    
    public function authenticate($usuario='', $contrasena=''){
        $stmt = $this->mysqli->prepare(
                "SELECT id FROM usuarios WHERE usuario='$usuario' AND contrasena='$contrasena'");
        $stmt->execute();
        $result = $stmt->get_result();
        $usuario = $result->fetch_all(MYSQLI_ASSOC);
//        var_dump($usuario[0]["id"]);
        $stmt->close();
        if ($usuario) {
            return $usuario[0]["id"];
        } else {
            return false;
        }
    }*/
}