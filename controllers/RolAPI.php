<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RolAPI
 *
 * @author WebDev
 */
class RolAPI extends EntityAPI {
    const API_ACTION = 'roles';
    
    public function __construct() {
        $this->db = new rolDB();
        $this->fields = [];
        array_push($this->fields, 
                'rol');
    }

    function processGet(){
        if($_GET['action']==self::API_ACTION){
            if(isset($_GET['id'])){
                $response = $this->db->getById($_GET['id']);
                echo json_encode($response,JSON_PRETTY_PRINT);
            }else{
                $response = $this->db->getList();
                echo json_encode($response,JSON_PRETTY_PRINT);
            }
        }else{
                $this->response(400);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->rol);
        if($r) {$this->response(200,"success", $r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $obj = json_decode(file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id, $obj->rol);
        if($r) { $this->response(200,"success",$id); }
        else { $this->response(204,"success","Record not updated");}
    }
}