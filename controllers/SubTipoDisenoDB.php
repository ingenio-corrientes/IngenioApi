<?php
/**
 * Description of SubTipoDisenoDB
 *
 * @author meza
 */
class SubTipoDisenoDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'subtiposdisenos';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=$id;");
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $result = $this->mysqli->query("SELECT s.id, s.idtipodiseno, d.tipodiseno, 
                s.subtipodiseno
            FROM subtiposdisenos s
            LEFT JOIN tiposdisenos d ON d.id =  s.idtipodiseno");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
	
    public function insert($idtipodiseno, $subtipodiseno){
        $stmt = $this->mysqli->prepare(
                "INSERT INTO " . self::TABLE . " (idtipodiseno, subtipodiseno) "
                . "VALUES ($idtipodiseno, '$subtipodiseno');");
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $idtipodiseno, $subtipodiseno) {
        $query = "UPDATE " . self::TABLE . " SET idtipodiseno=$idtipodiseno,"
                . "subtipodiseno='$subtipodiseno' "
                . "WHERE id = $id;";
//            var_dump($query);
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
	
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
