<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RolDB
 *
 * @author WebDev
 */
class RolDB extends EntityDB {
    protected $mysqli;
    const TABLE = 'roles';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM roles WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query('SELECT * FROM roles');
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($rol= ''){
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL new_rol('$rol', @p_oresult);";
//        var_dump($query);
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
//        $query="INSERT INTO roles (rol) VALUES ('$rol');";
//        $stmt = $this->mysqli->prepare($query);
//        $r = $stmt->execute();
//        $stmt->close();
//        $lastid = $this->mysqli->insert_id;
//        return $lastid;
    }
    
    public function update($id, $rol) {
        $query = "UPDATE roles SET rol='$rol' WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            
            return $r;    
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
