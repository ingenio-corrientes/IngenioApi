<?php
/**
 * Description of ReciboItemItemDB
 *
 * @author meza
 */
class ReciboItemDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'recibositems';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $query = "SELECT i.id, i.idrecibo, i.idformacobro, f.formacobro, i.idbanco, 
                b.banco, i.origendestino, i.librador, i.numero, 
                i.fecha, i.fecvencimiento, i.monto, i.fecultmodif 
            FROM recibositems i 
            LEFT JOIN formascobro f ON f.id = i.idformacobro 
            LEFT JOIN bancos b ON b.id = i.idbanco;";
//        var_dump($query);
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdRecibo($idrecibo=0){
        $query = "SELECT i.id, i.idrecibo, i.idformacobro, f.formacobro, i.idbanco, 
                IFNULL(b.banco, '') AS banco, i.origendestino, i.librador, i.numero, 
                i.fecha, i.fecvencimiento, i.monto, i.fecultmodif 
            FROM recibositems i 
            LEFT JOIN formascobro f ON f.id = i.idformacobro 
            LEFT JOIN bancos b ON b.id = i.idbanco 
            WHERE i.idrecibo = $idrecibo;";
//        var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function insert(
            $idrecibo=-1, $idformacobro=-1, $idbanco=-1, 
            $origendestino='', $librador='', $numero=-1,
            $fecha='', $fecvencimiento='', $monto=-1){
        
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_reciboitem(0, -1, $idrecibo, 
                $idformacobro, $idbanco, '$origendestino',
                '$librador', '$numero', '$fecha',
                '$fecvencimiento', $monto, @p_oresult);";
//        var_dump($query);
//        return 1;
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
    
    public function update($id=-1, 
            $idrecibo=-1, $idformacobro=-1, $idbanco='', 
            $origendestino='', $librador='', $numero='', 
            $fecha='', $fecvencimiento='', $monto=-1) {
        
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_reciboitem(1, $id, $idrecibo, 
                $idformacobro, $idbanco, '$origendestino', 
                '$librador', '$numero', '$fecha',
                '$fecvencimiento', $monto, @p_oresult);";
//        var_dump($query);
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    } 
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("SET @p_oresult:= -1");
        $stmt->execute();
        
        $query = "CALL abm_reciboitem(2, $id, -1, 
            -1, -1, '', 
            '', '', '', 
            '', -1, @p_oresult);";
//        var_dump($query);
//        return true;
        $r = $this->mysqli->query($query);
        
        $r = $this->mysqli->query('SELECT @p_oresult as p_oresult');
        $row = $r->fetch_assoc();
        return $row['p_oresult'];
    }
}