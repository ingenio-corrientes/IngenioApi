<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CargaTipoDB
 *
 * @author meza
 */
class CargaTipoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'cargastipos';
       
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT id, tipo, fecultmodif "
                . "FROM cargastipos ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($tipo='', $fecultmodif=''){
		$query= "INSERT INTO " . self::TABLE . " (tipo, fecultmodif) "
                . "VALUES ('$tipo', NOW());";
		//var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    public function update($id=-1, $tipo='', $fecultmodif='') {
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare(
                    "UPDATE " . self::TABLE . " SET tipo= '$tipo', fecultmodif= NOW() "
                    . "WHERE id = $id;");
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }

}