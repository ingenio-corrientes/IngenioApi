<?php
/**
 * Description of FormaPagoDB
 *
 * @author meza
 */
class FormaPagoDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'formaspago';
    
    public function getById($id=0){
        $stmt = $this->mysqli->prepare("SELECT * FROM " 
                . self::TABLE . " WHERE id=?;");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
    
    public function getList(){
        $result = $this->mysqli->query(
                "SELECT * FROM formaspago ");
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($formapago='', $bancario, $cheque){
        $query= "INSERT INTO " . self::TABLE . " (formapago, bancario, cheque, fecultmodif) "
        . "VALUES ('$formapago', $bancario, $cheque, NOW());";
        //var_dump($query);
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();
        
        $stmt->close();
        return $r;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = ?;");
        $stmt->bind_param('i', $id);
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, $formapago='', $bancario, $cheque) {
        $query = "UPDATE " . self::TABLE . " SET formapago= '$formapago', bancario = $bancario, cheque=$cheque, fecultmodif= NOW() "
                    . "WHERE id = $id;";
//            var_dump($query);
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
}