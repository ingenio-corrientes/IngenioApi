<?php
/**
 * Description of LPrecioAdicionalDB
 *
 * @author meza
 */
class LPrecioAdicionalDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'lpreciosadicionales';
    
    public function getList(){
        $query = "SELECT l.id, l.idlistaprecio, l.idtipoadicional,
                a.tipoadicional, l.precio, l.fecultmodif
            FROM lpreciosadicionales l
            LEFt JOIN tiposadicionales a ON a.id = l.idtipoadicional";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getById($id=0){
        $query = "SELECT l.id, l.idlistaprecio, l.idtipoadicional,
                a.tipoadicional, l.precio, l.fecultmodif
            FROM lpreciosadicionales l
            LEFt JOIN tiposadicionales a ON a.id = l.idtipoadicional
            WHERE l.id=$id;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdLista($idlista) {
        $query = "SELECT l.id, l.idlistaprecio, l.idtipoadicional,
                a.tipoadicional, l.precio, l.fecultmodif
            FROM lpreciosadicionales l
            LEFt JOIN tiposadicionales a ON a.id = l.idtipoadicional
            WHERE l.idlistaprecio = $idlista";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function insert($idlistaprecio, $idtipoadicional, $precio){
        $query = "INSERT INTO " . self::TABLE . " "
                . "(idlistaprecio, idtipoadicional, precio, fecultmodif) "
                . "VALUES "
                . "($idlistaprecio, $idtipoadicional, $precio, NOW());";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();        
        $stmt->close();
        $lastid = $this->mysqli->insert_id;
        return $lastid;
    }
    
    public function update($id=-1, 
            $idlistaprecio, $idtipoadicional, $precio) {
        $query = "UPDATE " . self::TABLE . " SET "
                . "idlistaprecio=$idlistaprecio, idtipoadicional=$idtipoadicional, precio=$precio, "
                . "fecultmodif=NOW() "
                . "WHERE id = $id;";
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $stmt = $this->mysqli->prepare("DELETE FROM ". self::TABLE ." WHERE id = $id;");
        $r = $stmt->execute(); 
        $stmt->close();
        return $r;
    }
}