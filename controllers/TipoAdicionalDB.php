<?php
/**
 * Description of TipoAdicionalDB
 *
 * @author meza
 */
class TipoAdicionalDB extends EntityDB {
   protected $mysqli;
   const TABLE = 'tiposadicionales';
    
    public function getById($id=0){
        $query = "SELECT t.id, t.idtiposervicio, s.servicio, 
                t.tipoadicional, t.anchomin, t.altomin, t.unidad
            FROM tiposadicionales t
            LEFT JOIN tiposservicios s ON s.id = t.idtiposervicio
            WHERE t.id=$id;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function getList(){
        $query = "SELECT t.id, t.idtiposervicio, s.servicio, 
                t.tipoadicional, t.anchomin, t.altomin, t.unidad
            FROM tiposadicionales t
            LEFT JOIN tiposservicios s ON s.id = t.idtiposervicio;";
        $result = $this->mysqli->query($query);
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $result->close();
        return $entity;
    }
    
    public function getByIdServicio($idservicio=0){
        $query = "SELECT t.id, t.idtiposervicio, s.servicio, 
                t.tipoadicional, t.anchomin, t.altomin, t.unidad
            FROm tiposadicionales t
            LEFT JOIN tiposservicios s ON s.id = t.idtiposervicio
            WHERE t.idtiposervicio=$idservicio;";
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        $entity = $result->fetch_all(MYSQLI_ASSOC);
        $stmt->close();
        return $entity;
    }
	
    public function insert($idtiposervicio, $tipoadicional, $anchomin, 
            $altomin, $unidad){
        $query = "INSERT INTO tiposadicionales 
                (idtiposervicio, tipoadicional, anchomin, altomin, unidad) 
            VALUES 
                ($idtiposervicio, '$tipoadicional', $anchomin, $altomin, $unidad);";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute();

        $stmt->close();
        return $r;
    }
    
    public function update($id=-1, 
            $idtiposervicio, $tipoadicional, $anchomin, 
            $altomin, $unidad) {
        $query = "UPDATE tiposadicionales SET 
                idtiposervicio = $idtiposervicio, tipoadicional = '$tipoadicional', anchomin = $anchomin, 
                altomin = $altomin, unidad = $unidad 
            WHERE id = $id;";
//        var_dump($query);
        if($this->checkIntID(self::TABLE, $id)){
            $stmt = $this->mysqli->prepare($query);
            $r = $stmt->execute(); 
            $stmt->close();
            return $r;
        }
        return false;
    }
    
    public function delete($id=0) {
        $query = "DELETE FROM ". self::TABLE ." WHERE id = $id;";
        $stmt = $this->mysqli->prepare($query);
        $r = $stmt->execute(); 
        $errno = $this->mysqli->errno;
    //    var_dump($errno);
        $stmt->close();
        if($errno > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}
