<?php
/**
 * Description of GastoAPI
 *
 * @author meza
 */
class GastoAPI extends EntityAPI {
    const API_ACTION = 'gasto';
    const GET_FILTEREDBYDATES = 'filteredbydates';
    const PUT_EMITIR = 'emitir';
    const PUT_PAGAR = 'pagar';

    public function __construct() {
	$this->db = new GastoDB();
        $this->fields = [];
        array_push($this->fields, 
                'idproveedor',
                'fecha',
                'concepto');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isFilteredByDates = isset($id) ? $id === self::GET_FILTEREDBYDATES : false;
        
        if($isFilteredByDates) {
            $fecinicio = filter_input(INPUT_GET, 'fld1');
            $fecfin = filter_input(INPUT_GET, 'fld2');
            $response = $this->db->getFilteredByDates($fecinicio, $fecfin);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($id){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(isset($obj->idproveedor) AND isset($obj->fecha) 
                AND isset($obj->concepto)) {
            $r = $this->db->insert(
                    $obj->idproveedor, $obj->fecha, $obj->concepto);
//            var_dump($r);
            if($r) { $this->response(200, "success", $r); }
        } else {
            $this->response(422, "error", "The property is not defined");
        }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        if(!$id){
            $this->response(400, "error", "The id not defined");
            exit;
        }
        
        $isEmitir = isset($id) ? $id === self::PUT_EMITIR : false;
        $isPagar = isset($id) ? $id === self::PUT_PAGAR : false;
        if($isEmitir) {
            $this->processPutEmitir();
        } elseif($isPagar) {
            $this->processPutPagar();
        } else {
            $this->processPutGral($id);
        }
    }
    
    function processPutEmitir() {
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if (empty($objArr)){                        
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        $r = $this->db->emitir($obj->id);
//        var_dump($r);
        if($r > 0) {
            $this->response(200,"success",$r);
        } else {
            $this->response(304,"error", $r);
        }
    }
    
    function processPutGral($id=-1) {
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if (empty($objArr)){                        
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        
        if(isset($obj->idproveedor) AND isset($obj->fecha) AND isset($obj->concepto)){
            if($this->db->update($id,
                $obj->idproveedor, $obj->fecha, $obj->concepto)) {
                $this->response(200,"success",$id);                             
            } else {
                $this->response(304,"success","Record not updated");
            }
        }else{
            $this->response(422,"error","The property is not defined");                        
        }
    }
    
    function processPutPagar() {
        $obj = json_decode( file_get_contents('php://input') );   
        $objArr = (array)$obj;
        if (empty($objArr)){                        
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->pagar($obj->idproveedor, $obj->idcompraapagar);
        
        if($r) { $this->response(200,"success", $r); }
        else { $this->response(204,"success","Record not updated");}
    }
}