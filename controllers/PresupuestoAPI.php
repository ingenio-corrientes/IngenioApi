<?php
/**
 * Description of PresupuestoAPI
 *
 * @author meza
 */
class PresupuestoAPI extends EntityAPI {
    const GET_PENDIENTESBYIDCLIENTE = 'pendbyidcliente';
    const GET_ITEMSBYID = 'itemsbyid';
    const GET_ITEMSADSBYID = 'itemsadsbyid';
    const PUT_EMITIR = 'emitir';
    const PUT_APRODUCCION = 'aproduccion';
    const API_ACTION = 'presupuesto';
    const PUT_APLICARDESREC = 'aplicardescrec';

    public function __construct() {
        $this->db = new PresupuestoDB();
        $this->fields = [];
        array_push($this->fields, 
                'idusuario', 
                'idcliente', 
                'idformacobro', 
                'fecemision',  
                'observacion', 
                'descuyrecar', 
                'fecentrega',  
                'fecaproduccion',
                'saldo',
                'actacte',
                'dirty');
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        $isPendByIdCliente = strpos($id, self::GET_PENDIENTESBYIDCLIENTE);
        $isItemsById = strpos($id, self::GET_ITEMSBYID);
        $isItemsAdsById = strpos($id, self::GET_ITEMSADSBYID);
        
        if($isPendByIdCliente !== false) {
            $idcliente = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getPendientesByIdCliente($idcliente);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isItemsById !== false) {
            $idpresup = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getItemsById($idpresup);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif($isItemsAdsById !== false) {
            $idpresup = filter_input(INPUT_GET, 'fld1');
            $response = $this->db->getItemsAdsById($idpresup);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } elseif(isset($id)){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        } else {
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
            exit;
        }        
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $r = $this->db->insert($obj->idusuario, $obj->idcliente, $obj->idformacobro, 
                $obj->fecemision, $obj->observacion, $obj->descuyrecar, 
                $obj->fecentrega, $obj->fecaproduccion, $obj->saldo, 
                $obj->actacte, $obj->dirty);
        if($r) {$this->response(200,"success",$r); }
        else {$this->response(204,"error","No record added"); }
    }
    
    function processPut() {
        $id = filter_input(INPUT_GET, 'id');
        $isEmitir =  isset($id) ? ($id === self::PUT_EMITIR) : false;
        $isAProduccion =  isset($id) ? ($id === self::PUT_APRODUCCION) : false;
        $isAplicarDescRec =  isset($id) ? ($id === self::PUT_APLICARDESREC) : false;
        
        if ($isEmitir) {
            $this->processEmitir();
        } elseif ($isAProduccion) {
            $this->processAProduccion();
        } elseif ($isAplicarDescRec) { 
            $this->processAplicarDescRec();
        } else {
            $this->processStandartPut();
        }
    }
    
    function processStandartPut() {
        $obj = json_decode( file_get_contents('php://input') );
        if(!$this->checkFields($obj)) {
            $this->response(422,"error","The property is not defined");
            exit;
        }
        $id = filter_input(INPUT_GET, 'id');
        if(!$id) {
            $this->response(422,"error","Id no enviado.");
            exit;
        }
        $r = $this->db->update($id,
                $obj->idusuario, $obj->idcliente, $obj->idformacobro, 
                $obj->fecemision, $obj->observacion, $obj->descuyrecar, 
                $obj->fecentrega, $obj->fecaproduccion, $obj->saldo, 
                $obj->actacte, $obj->dirty);
        if($r) { $this->response(200,"success",$id); }
        else { $this->response(204,"success","Record not updated");}
    }
    
    function processEmitir() {
        $id = filter_input(INPUT_GET, 'fld1');
        $r = $this->db->emitir($id);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
    
    function processAProduccion() {
        $id = filter_input(INPUT_GET, 'fld1');
        $r = $this->db->aProduccion($id);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
    
    function processAplicarDescRec() {
        $id = filter_input(INPUT_GET, 'fld1');
        $obj = json_decode( file_get_contents('php://input') );
        $r = $this->db->aplicarDescRec($id, $obj->descuyrecar);
        if($r) { $this->response(200,"success","Record updated"); }
        else { $this->response(204,"success","Record not updated");}
    }
}