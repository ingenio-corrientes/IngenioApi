<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RetiroEfectivoAPI
 *
 * @author meza
 */
class RetiroEfectivoAPI extends EntityAPI {
    const API_ACTION = 'retiroefectivo';

    public function __construct() {
	$this->db = new RetiroEfectivoDB();
    }
    
    function processGet(){
        $id = filter_input(INPUT_GET, 'id');
        if(isset($id)){
            $response = $this->db->getById($id);
            echo json_encode($response,JSON_PRETTY_PRINT);
        }else{
            $response = $this->db->getList();
            echo json_encode($response,JSON_PRETTY_PRINT);
        }
    }
    
    function processPost() {
        $obj = json_decode( file_get_contents('php://input') );
        $objArr = (array)$obj;
        if (empty($objArr)) {
            $this->response(422,"error","Nothing to add. Check json");
        } else if(isset($obj->idresponsable) AND isset($obj->fecha) 
                AND isset($obj->concepto) AND isset($obj->monto)) {
            $r = $this->db->insert($obj->idresponsable, $obj->fecha, 
                    $obj->concepto, $obj->monto);
            if($r) { $this->response(200,"success","new record added"); }
        } else {
            $this->response(422,"error","The property is not defined");
        }
    }
    
    function processPut() {
        if( isset($_GET['action']) && isset($_GET['id']) ){
            if($_GET['action']==self::API_ACTION){
                $obj = json_decode( file_get_contents('php://input') );   
                $objArr = (array)$obj;
                if (empty($objArr)){                        
                    $this->response(422,"error","Nothing to add. Check json");                        
                }else if(isset($obj->idresponsable) AND isset($obj->fecha) 
                    AND isset($obj->concepto) AND isset($obj->monto)){
                    if($this->db->update($_GET['id'],$obj->idresponsable, $obj->fecha, 
                        $obj->concepto, $obj->monto))
                        $this->response(200,"success","Record updated");                             
                    else
                        $this->response(304,"success","Record not updated");                             
                }else{
                    $this->response(422,"error","The property is not defined");                        
                }     
                exit;
           }
        }
        $this->response(400);
    }
}
